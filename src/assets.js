const burger = require('./assets/burger.png');
const config = require('./assets/config.png');
const logo = require('./assets/maglogo2.jpg');
const user = require('./assets/user.png');
const circle = require('./assets/circle.png');
const circleTick = require('./assets/circleTick.png');
const home = require('./assets/home.png');
const back = require('./assets/back.png');
const checked = require('./assets/checked.png');
const unchecked = require('./assets/unchecked.png');
const calendar = require('./assets/calendar.png');
const deleteIcon = require('./assets/delete.png');
const carTopView = require('./assets/car-top-view.png');
const image1 = require('./assets/1.jpg');
const image2 = require('./assets/2.jpg');
const image3 = require('./assets/3.jpg');
const cameraCircle = require('./assets/camera-circle.png');
const cross = require('./assets/cross.png');
const imageIcon = require('./assets/imageIcon.png');
const pointer = require('./assets/pointer.png');
const rightArrow = require('./assets/right_arrow.png');


export {
    burger, config, logo, user, circle, circleTick, home, back, checked, unchecked, calendar, deleteIcon,
    carTopView, image1, image2, image3, cameraCircle, cross, imageIcon, pointer, rightArrow
}