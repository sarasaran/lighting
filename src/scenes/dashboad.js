import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import { Container, Content, Row } from 'native-base';
import { AfterLoginHeader, DashCards, ClaimsRow2, Loader, ButtonC } from '../components';
import isTablet from '../utility/deviceType';
import { burger } from '../assets';
import { burgerDiv, burgerImage, dashBoardCardContainer, dashBoardHighlightedText, noclaimsDiv, noclaimsText } from '../styling';
import { connect } from 'react-redux';
import { getDashboardData } from '../store/Dashboard';
class Dashboard extends Component {


    constructor(props) {
        super(props)

        this.state = {
            dealerKey: null,
            contactid: null
        }
    }
   
       componentWillMount() {
        const { params } = this.props.navigation.state;
        const dealerKey = params ? params.dealerKey : null
        const contactid = params ? params.contactid : null
        const { getDashboardData } = this.props;
        if (dealerKey) {
            this.setState({
                dealerKey,
                contactid
            })
            var body = { dealerkey: dealerKey, contactid: contactid }
           
            getDashboardData(body)
        }
    }

    _renderClaims = (data) => {
        if (data) {
            return data.map((v, i) => {
                return <ClaimsRow2 onPress={() => this.getClaimIntent(v, i)} productgroup={v.productgroup} title={v.firstname + " " + v.lastname} createdDate={v.createdDate} dealer={v.dealer} key={'claims' + i} claims={v.issuestatus} />
            })
        } else {
            return (
                <View style={noclaimsDiv}>
                    <Text style={noclaimsText}>No Claims</Text>
                </View>
            )
        }
    }

    getClaimIntent(v, i) {
       
        this.props.navigation.navigate('ClaimIntent', { contractid: v.contractid })
    }

    render() {

        const { isLoading, dashData } = this.props;
        var totalClaimsInMonth = dashData ? dashData.totalClaimsInMonth : 0
        var totalClaimsInYear = dashData ? dashData.totalClaimsInYear : 0
        var totalPaidInMonth = dashData ? dashData.totalPaidInMonth : 0
        var totalPaidInYear = dashData ? dashData.totalPaidInYear : 0
        var openClaims = dashData ? dashData.openClaims : null
        var unAssignedClaims = dashData ? dashData.unAssignedClaims : null
        var last30dayspaidclaims = dashData ? dashData.last30dayspaidclaims : null

        return (
            <Container>
                <AfterLoginHeader title={'Global Service View'} onPress={() => this.props.navigation.navigate('AdvisorManager')} />
                <Content>
                    <View style={{ flex: 1 }}>
                        <View style={{ width: '100%', alignItems: 'center' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} style={burgerDiv}>
                                <Image source={burger} style={burgerImage} />
                            </TouchableOpacity>
                            <View style={dashBoardCardContainer}>
                                <DashCards topText={totalClaimsInMonth} subText={'Total Claims Paid \n Month To Date'} />
                                <DashCards topText={'$' + totalPaidInMonth} subText={'Total Dollars Paid \n  Month To Date'} />
                                <DashCards topText={totalClaimsInYear} subText={'Total claims Paid \n Year To Date'} />
                                <DashCards topText={'$' + totalPaidInYear} subText={'Total Dollars Paid \n Year To Date'} />
                                <ButtonC style={{ width: '100%' }} onPress={this.submit} title={'SCAN VIN'} />
                            </View>
                        </View>
                        <Text style={dashBoardHighlightedText}>All Open Claims</Text>
                        {this._renderClaims(openClaims)}
                        <Text style={dashBoardHighlightedText}>Unassigned Claims</Text>
                        {this._renderClaims(unAssignedClaims)}
                        <Text style={dashBoardHighlightedText}>Last 30 Days Paid Claims</Text>
                        {this._renderClaims(last30dayspaidclaims)}
                    </View>
                </Content>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}


const mapStateToProps = state => ({
    error: state.Dashboard.error,
    isLoading: state.Dashboard.isLoading,
    dashData: state.Dashboard.dashData
})

const mapDispatchToProps = {
    getDashboardData: getDashboardData
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)