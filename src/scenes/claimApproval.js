import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, TextInput, Alert, ScrollView } from 'react-native';
import { Container, Content, Toast } from 'native-base';
import { dWidth, fontSize } from '../styling';
import { buttonText, buttonDiv } from '../styling';
import isTablet from '../utility/deviceType';
import { AfterLoginHeader2, Loader, ClaimIntentTopView, ButtonC, ConfirmClaimApproval, ImagePickerPopup, ImagePreview, CopyDataFrom, Claimcartdetails } from '../components';
import { connect } from 'react-redux';
import { calendar, carTopView } from '../assets';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { formatDate } from '../utility/date';
import { CardImagedisplay } from '../components';
import { getClaim } from '../store/claimApproval';
const userInfoDiv = { flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center' }
const highlightedText = { height: isTablet ? 62 : 40, fontSize: isTablet ? 26 : 18, textAlign: 'center', backgroundColor: '#535353', color: '#FFFFFF', padding: isTablet ? 17 : 6 }
const multilineText = { minHeight: isTablet ? 100 : 60, width: isTablet ? '48%' : '100%', borderColor: '#8492A6', borderWidth: 0.5, borderRadius: 2.5, marginBottom: isTablet ? 0 : 5, fontSize: fontSize, padding: isTablet ? 10 : 5, textAlignVertical: 'top' }
const bottomDiv = { zIndex: 100, height: isTablet ? 80 : 60, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor: '#535353', paddingHorizontal: isTablet ? 20 : 10, position: 'absolute', left: 0, right: 0, bottom: 0 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }
import { ImagePicker, Permissions } from 'expo';
import axios from 'axios';
import { deleteIcon } from '../assets';
import { logo, config } from '../assets';
import { apiSource } from '../constants';
const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});
class claimApproval extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isSubmitConfirmShow: false,
            dateofDamage: 'Date of Damage/Loss',
            isDatePicker: false,
            contractid: "contractid",
            damagedescription: "",
            generalnotes: "",
            claimid: null,
            fullViewImageData: null,
            showImagePicker: false,
            showImagePreview: false,
            showCopyDataPopup: false,
            claimresult: null,
            tireIssues: 0,
            wheelIssues: 0,
            totalRequest: 0,
            amtAuth: 0,
            totalAmt: 0,
            claimsts: 'status',
            driverFront: [],
            passengerFront: [],
            driverRear: [],
            passengerRear: [],
            imageArray: [],
            inactive_re_pas: 'rgba(162,162,162,0.27)',
            inactive_re_frnt: 'rgba(162,162,162,0.27)',
            active_frnt: 'rgba(31,200,83,0.27)',
            inactive_pass: 'rgba(162,162,162,0.27)',
            isLoading: true,
            attachro: [],
            documentsshow: false,
            status: '',
            totalcostfront: 0,
            totalcostpass: 0,
            totalcostfrontrear: 0,
            totalcostpassrear: 0,
            imgbase: []

        }

        this._showDateTimePicker = this._showDateTimePicker.bind(this)
        this._hideDateTimePicker = this._hideDateTimePicker.bind(this)
        this._handleDatePicked = this._handleDatePicked.bind(this)
    }
    componentWillReceiveProps(newProps) {
        const { navigation } = this.props;

        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.message) {
                    console.log(message);
                    console.log('***************************************');
                    console.log(newProps.data);
                    this.showToast(newProps.message.message)

                }
            }
        }
    }
    async componentWillMount() {
        const { params } = this.props.navigation.state;
        const claimid = params ? params.claimsdetails : null;
        const cameraPermission = await Expo.Permissions.askAsync(Expo.Permissions.CAMERA);
        const cameraRollPermission = await Expo.Permissions.askAsync(Expo.Permissions.CAMERA_ROLL);
        var attachid = '';
        if (claimid) {
            this.setState({ claimid: 'a0A0v0000015zOx' })
        }
        this.setState({ isLoading: true });
        //     var body = {claimid: 'a0A0v0000015zTA' }
        //    getClaim(body);
        //axios.get('https://rallycoding.herokuapp.com/api/music_albums') .then(response => console.log(response));
        axios.get('https://dev3-mag.cs66.force.com/customer/services/apexrest/services/getclaimdetails?claimid=' + this.state.claimid)

            .then(response => {
                let result = response.data;
                this.setState({ claimresult: result });
                this.setState({
                    dateofDamage: this.state.claimresult.dateofdamage,
                    damagedescription: this.state.claimresult.damagedescription,
                    generalnotes: this.state.claimresult.generalnotes,
                    claimsts: this.state.claimresult.status
                });
                this.setState({ claimsts: this.state.claimresult.status });
                this.setState({ status: this.state.claimresult.status })
                console.log(this.state.claimresult.status + "ss");
                console.log(this.state.claimresult.claimissues.length)
                var i = 0;
                var total = 0;
                for (i; i < this.state.claimresult.claimissues.length; i++) {
                    total = total + this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax;
                    if (this.state.claimresult.claimissues[i].issuetype == 'Tire' && this.state.claimresult.claimissues[i].damagelocation == 'Driver Front') {

                        arraypush = [];
                        if (this.state.driverFront.length > 0) {
                            arraypush.push(this.state.driverFront[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ driverFront: arraypush });
                        this.setState({ totalcostfront: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax });

                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Wheel' && this.state.claimresult.claimissues[i].damagelocation == 'Driver Front') {
                        arraypush = [];
                        if (this.state.driverFront.length > 0) {
                            arraypush.push(this.state.driverFront[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ driverFront: arraypush });
                        this.setState({ totalcostfront: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax + this.state.totalcostfront });
                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Tire' && this.state.claimresult.claimissues[i].damagelocation == 'Passenger Front') {

                        arraypush = [];
                        if (this.state.passengerFront.length > 0) {
                            arraypush.push(this.state.passengerFront[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ passengerFront: arraypush });
                        this.setState({ totalcostpass: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax });
                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Wheel' && this.state.claimresult.claimissues[i].damagelocation == 'Passenger Front') {
                        arraypush = [];
                        if (this.state.passengerFront.length > 0) {
                            arraypush.push(this.state.passengerFront[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ passengerFront: arraypush });
                        this.setState({ totalcostpass: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax + this.state.totalcostpass });
                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Tire' && this.state.claimresult.claimissues[i].damagelocation == 'Driver Rear') {
                        arraypush = [];
                        if (this.state.driverRear.length > 0) {
                            arraypush.push(this.state.driverRear[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ driverRear: arraypush });
                        this.setState({ totalcostfrontrear: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax });
                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Wheel' && this.state.claimresult.claimissues[i].damagelocation == 'Driver Rear') {
                        arraypush = [];
                        if (this.state.driverRear.length > 0) {
                            arraypush.push(this.state.driverRear[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ driverRear: arraypush });
                        this.setState({ totalcostfrontrear: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax + this.state.totalcostfrontrear });

                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Tire' && this.state.claimresult.claimissues[i].damagelocation == 'passenger Rear') {
                        arraypush = [];
                        if (this.state.passengerRear.length > 0) {
                            arraypush.push(this.state.passengerRear[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ passengerRear: arraypush });
                        this.setState({ totalcostpassrear: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax });
                    }
                    else if (this.state.claimresult.claimissues[i].issuetype == 'Wheel' && this.state.claimresult.claimissues[i].damagelocation == 'passenger Rear') {
                        arraypush = [];
                        if (this.state.passengerRear.length > 0) {
                            arraypush.push(this.state.passengerRear[0]);
                        }
                        arraypush.push(this.state.claimresult.claimissues[i]);
                        this.setState({ passengerRear: arraypush });
                        this.setState({ totalcostpassrear: this.state.claimresult.claimissues[i].partscost + this.state.claimresult.claimissues[i].laborcost + this.state.claimresult.claimissues[i].othercost + this.state.claimresult.claimissues[i].tax + this.state.totalcostpassrear });
                    }

                }

                this.setState({ totalAmt: total });
                if (this.state.driverFront.length > 0)
                    this._cardClick("driverFront", this.state.driverFront);
                else if (this.state.passengerFront.length > 0)
                    this._cardClick("passengerFront", this.state.passengerFront);
                else if (this.state.driverRear.length > 0)
                    this._cardClick("driverRear", this.state.driverRear);
                else if (this.state.passengerRear.length > 0)
                    this._cardClick("passengerRear", this.state.passengerRear);

                this.setState({ isLoading: false });
            }
            );



    }

    submitAll() {
        this.setState({ isSubmitConfirmShow: true })
    }
    async picker(camera) {
        this.setState({ showImagePicker: false })
        if (camera) {
            let captureImage = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 6],
                base64: true,
            });

            if (!captureImage.cancelled) {
                let Image = captureImage;
                this.setImage(Image);
            }


        } else {
            let captureGalleryImage = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 6],
                base64: true,
            });

            if (!captureGalleryImage.cancelled) {
                let GalleryImage = captureGalleryImage;
                this.setImage(GalleryImage);
            }
        }

    }
    setImage(image) {
        console.log("START" + this.state.imageArray.length);
        const { changeImageIndex } = this.state;

        var arrayimg = [];
        var arrayimgbase = [];
        if (this.state.imageArray.length > 0) {
            var len = 0;
            console.log('loop');
            for (len; len < this.state.imageArray.length; len++) {
                console.log('loopin');
                arrayimg.push(this.state.imageArray[len]);
                arrayimgbase.push(this.state.imgbase[len]);
            }


        }

        arrayimg.push(image);
        arrayimgbase.push(image.base64);

        this.setState({ imageArray: arrayimg })
        this.setState({ imgbase: arrayimgbase })
        console.log(this.state.imageArray.length);
        console.log(arrayimg.length);
    }
    onImageClick(image, index) {

        if (image) {
            this.setState({ activePreviewImage: image, showImagePreview: true })
        } else {
            this.setState({ changeImageIndex: index, showImagePicker: true })
        }
    }
    _showDateTimePicker() {
        this.setState({ isDatePicker: true });
    }

    _hideDateTimePicker() {
        this.setState({ isDatePicker: false });
    }

    _handleDatePicked = (date) => {
        this.setState({
            dateofDamage: formatDate(date)
        })
        this._hideDateTimePicker();
    };

    removeClaim(index) {
        Alert.alert(
            'Alert',
            'are you sure you wish to remove?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this.deletionConfirmed(index) }
            ]
        )
    }
    deletionConfirmed(index) {
        console.log(index);
        this.setState({
            imageArray: this.state.imageArray.filter((_, i) => i !== index),
            imgbase: this.state.imgbase.filter((_, i) => i !== index)
        });

    }
    actionSubmit() {
        console.log('hitted')
        console.log(this.state.claimid);
        var len = 0;
        var attachmentList = [];
        for (len; len < this.state.imageArray.length; len++) {

            var arr = {
                "attachmentname": "img_" + len,
                "file": this.state.imgbase[len]
            }
            attachmentList.push(arr);
        }
        console.log(attachmentList.length);
        var body = { "claimid": this.state.claimid, attachmentList }

        api.post('submitclaims', body)
            .then(response => {

                let data = response.data;
                console.log(data);
                if (data) {
                    if (data.success == true) {
                        this.setState({
                            isSubmitConfirmShow: false,
                            documentsshow: true
                        })
                    }
                }

            });


    }
    _cardClick = (side, data) => {

        var tlrqsttx = 0;
        var len = 0;

        for (len; len < data.length; len++) {
            if (data[len].issuetype == 'Tire') {
                this.setState({ tireIssues: 1 });
            }
            else if (data[len].issuetype == 'Wheel') {
                this.setState({ wheelIssues: 1 });
            }
            tlrqsttx = tlrqsttx + data[len].partscost + data[len].laborcost + data[len].othercost + data[len].tax;


        }


        if (side == 'driverFront' && data.length > 0) {
            this.setState({ totalRequest: tlrqsttx, amtAuth: tlrqsttx });
            this.setState({ inactive_re_pas: 'rgba(162,162,162,0.27)', inactive_re_frnt: 'rgba(162,162,162,0.27)', inactive_pass: 'rgba(162,162,162,0.27)', active_frnt: 'rgba(31,200,83,0.27)' });
        }
        else if (side == 'passengerFront' && data.length > 0) {
            this.setState({ inactive_re_pas: 'rgba(162,162,162,0.27)', inactive_re_frnt: 'rgba(162,162,162,0.27)', inactive_pass: 'rgba(31,200,83,0.27)', active_frnt: 'rgba(162,162,162,0.27)' });
            this.setState({ totalRequest: tlrqsttx, amtAuth: tlrqsttx });

        } else if (side == 'driverRear' && data.length > 0) {
            this.setState({ totalRequest: tlrqsttx, amtAuth: tlrqsttx });
            this.setState({ inactive_re_pas: 'rgba(162,162,162,0.27)', inactive_re_frnt: 'rgba(31,200,83,0.27)', inactive_pass: 'rgba(162,162,162,0.27)', active_frnt: 'rgba(162,162,162,0.27)' });
        }
        else if (side == 'passengerRear' && data.length > 0) {
            this.setState({ inactive_re_pas: 'rgba(31,200,83,0.27)', inactive_re_frnt: 'rgba(162,162,162,0.27)', inactive_pass: 'rgba(162,162,162,0.27)', active_frnt: 'rgba(162,162,162,0.27)' });
            this.setState({ totalRequest: tlrqsttx, amtAuth: tlrqsttx });
        }

    }

    render() {

        const { showImagePicker, activePreviewImage, showImagePreview,
            showCopyDataPopup, type, isLoading
        } = this.state;
        const { claimData, navigation,
            claimImages0, claimImages1, claimImages2, claimImages3
        } = this.props;
        const { dateofDamage, isDatePicker, damagedescription, generalnotes, isSubmitConfirmShow } = this.state;
        const data = claimData ? claimData.data : null;
        const city = data ? data.city : null
        const customername = data ? data.customername : null
        const phone = data ? data.phone : null
        const state = data ? data.state : null
        const street = data ? data.street : null
        const vehicleInfo = data ? data.vehicleInfo : null
        const zipcode = data ? data.zipcode : null


        return (
            <Container >
                <AfterLoginHeader2 name={'Chris Henderson'} onPress={() => navigation.goBack()} isBack={true} />
                <Content>
                    <View style={{ flex: 1 }}>
                        <ClaimIntentTopView customername={customername} street={street} city={city} state={state} zipcode={zipcode} phone={phone} userInfoDiv={userInfoDiv} vehicleInfo={vehicleInfo} />
                        {/* {this.state.status == 'Success' ?  */}
                        <Text style={highlightedText}>Tire/Wheel Claim Approval Request</Text>
                        {/* : null} */}
                        {/* {this.state.status == 'Success' ?  */}
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                            <TouchableOpacity onPress={() => this.onImageClick()} style={[buttonDiv, { width: '30%', marginTop: 0 }]}>
                                <Text style={buttonText}>Attach RO</Text>
                            </TouchableOpacity>

                            <Text style={{ width: '60%', marginTop: 0 }}>Amount Authorized ${this.state.amtAuth}</Text>
                        </View>
                        {/* : null} */}

                        {/* {this.state.status == 'Success' ?  */}
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                            <View style={{ marginBottom: 100, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10, borderRadius: 8, borderStyle: 'dotted', borderColor: '#343F4B', borderWidth: 1, height: 100, left: 10, top: 5 }}>
                                {this.state.imageArray.map((i, v) => (
                                    <View style={{ marginTop: 0 }}>
                                        <TouchableOpacity onPress={() => this.removeClaim(v)} style={{ position: 'absolute', top: isTablet ? 10 : 5, zIndex: 100, right: isTablet ? 10 : 5, left: isTablet ? 10 : 5, marginTop: 0 }}>
                                            <Image source={deleteIcon} style={{ width: isTablet ? 32 : 18, height: isTablet ? 32 : 18, marginTop: 0 }} />
                                        </TouchableOpacity>

                                        <View style={{ padding: 5, justifyContent: 'space-between', marginTop: 15 }}>

                                            <Image source={i} style={{ width: isTablet ? 100 : 75, height: isTablet ? 100 : 75 }} />
                                        </View></View>))}
                            </View>
                        </ScrollView>
                        {/* : null} */}

                        <View style={bottomDiv}>
                            <ButtonC style={{ backgroundColor: '#9D1B34', width: '30%', marginTop: 0, alignItems: 'center' }} onPress={() => navigation.goBack()} title={'Cancel'} />
                            {/* {this.state.status == 'Success' ?  */}
                            <TouchableOpacity onPress={() => this.submitAll()} style={[buttonDiv, { backgroundColor: '#19831C', width: '50%', marginTop: 0 }]}>
                                <Text style={buttonText}>Submit For Approval</Text>
                            </TouchableOpacity>
                            {/* : null} */}

                        </View>
                    </View>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <Text style={highlightedText}>Tire/Wheel Claim Submitted</Text>
                        <View style={{ width: isTablet ? '40%' : '60%', marginTop: 15, marginLeft: isTablet ? 20 : 8, padding: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderColor: '#47525E', borderWidth: 0.5, borderRadius: 2.5 }}>
                            <Text style={{ width: '80%', color: '#47525E', fontSize: isTablet ? 22 : 16, marginRight: 15 }}>{dateofDamage}</Text>
                            <TouchableOpacity onPress={() => this.setState({ isDatePicker: true })}>
                                <Image source={calendar} style={{ width: isTablet ? 36 : 24, height: isTablet ? 36 : 24 }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: '100%', marginTop: 10, marginLeft: isTablet ? 20 : 8, marginLeft: isTablet ? 20 : 8, padding: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                            <TextInput
                                value={damagedescription}
                                onChangeText={(damagedescription) => this.setState({ damagedescription })}
                                multiline={true}
                                maxLength={500}
                                placeholder={'Describe how the damage/loss occurred'}
                                style={{ width: '45%', borderColor: '#47525E', borderWidth: 0.5, borderRadius: 2.5, height: 60, textAlignVertical: 'top' }}
                                underlineColorAndroid="transparent" />

                            <TextInput
                                value={generalnotes}
                                onChangeText={(generalnotes) => this.setState({ generalnotes })}
                                multiline={true}
                                maxLength={500}
                                placeholder={'General Notes'}
                                style={{ width: '45%', borderColor: '#47525E', borderWidth: 0.5, borderRadius: 2.5, height: 60, right: 20, textAlignVertical: 'top' }}
                                underlineColorAndroid="transparent" />


                        </View>


                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}>
                            <View style={{ width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={carTopView} style={{ width: '50%', height: '75%' }} resizeMode='contain' />
                                <View style={{ width: '50%', height: 0.5, position: 'absolute', left: '25%', top: '50%', backgroundColor: 'black' }}></View>
                                <View style={{ width: 0.5, height: '90%', position: 'absolute', left: '50%', top: '5%', backgroundColor: 'black' }}></View>
                            </View>
                            <Claimcartdetails onPress={() => this._cardClick("driverFront", this.state.driverFront)} align={'left'} title={'Driver Front'} data={this.state.driverFront} style={{ marginBottom: isTablet ? 40 : 20 }} total={this.state.totalcostfront} backgroundstyle={this.state.driverFront.length == 0 ? this.state.inactive_re_pas : this.state.active_frnt} />
                            <Claimcartdetails onPress={() => this._cardClick("passengerFront", this.state.passengerFront)} align={'right'} title={'Passenger Front'} data={this.state.passengerFront} style={{ marginBottom: isTablet ? 40 : 20 }} backgroundstyle={this.state.driverFront.length == 0 && this.state.driverRear.length == 0 && this.state.passengerRear.length == 0 ? this.state.active_frnt : this.state.inactive_pass} total={this.state.totalcostpass} />
                            <Claimcartdetails onPress={() => this._cardClick("driverRear", this.state.driverRear)} align={'left'} title={'Driver Rear'} data={this.state.driverRear} backgroundstyle={this.state.driverFront.length == 0 && this.state.passengerFront.length == 0 && this.state.passengerRear.length == 0 ? this.state.active_frnt : this.state.inactive_re_frnt} total={this.state.totalcostfrontrear} />
                            <Claimcartdetails onPress={() => this._cardClick("passengerRear", this.state.passengerRear)} align={'right'} title={'Passenger Rear'} data={this.state.passengerRear} backgroundstyle={this.state.driverFront.length == 0 && this.state.passengerFront.length == 0 && this.state.driverRear.length == 0 ? this.state.active_frnt : this.state.inactive_re_pas} total={this.state.totalcostpassrear} />
                        </View>
                        <View style={{ width: isTablet ? dWidth - 40 : dWidth - 20, marginLeft: isTablet ? 20 : 10, borderTopColor: '#B1B1B1', borderTopWidth: isTablet ? 4 : 2, marginTop: isTablet ? 20 : 10 }}></View>
                        <View style={{ padding: isTablet ? 20 : 10, marginBottom: isTablet ? 90 : 70, flexDirection: 'row', flexDirection: 'row', width: '100%' }}>
                            <View style={{ width: '50%', alignItems: 'flex-start' }}>
                                <Text style={smallText}>Tire Issues: ${this.state.tireIssues}</Text>
                                <Text style={smallText}>Wheel Issues: ${this.state.wheelIssues}</Text>
                                <Text style={smallText}>Total Requested: ${this.state.totalRequest}</Text>
                            </View>
                            <View style={{ width: '50%', alignItems: 'flex-end' }}>
                                <Text style={smallText}>Total Authorized: ${this.state.totalAmt}</Text>
                                <Text style={smallText}>Total Approved: {this.state.status}</Text>
                            </View>
                        </View>
                    </View>
                </Content>
                <Loader isLoading={isLoading} />
                <ConfirmClaimApproval Docvalues={this.state.imgbase.length} visible={isSubmitConfirmShow} backDrop={() => this.setState({ isSubmitConfirmShow: false })} cancel={() => this.setState({ isSubmitConfirmShow: false })} submit={() => this.actionSubmit()} />

                <Loader isLoading={isLoading} />
                <ImagePreview showImagePreview={showImagePreview} image={activePreviewImage} cancel={() => this.setState({ showImagePreview: false })} onYes={() => this.setState({ showImagePreview: false, showImagePicker: true })} backDrop={() => this.setState({ showImagePreview: false })} />
                <ImagePickerPopup showImagePicker={showImagePicker} backDrop={() => this.setState({ showImagePicker: false })} close={() => this.setState({ showImagePicker: false })} cameraClick={() => this.picker(true)} galleryClick={() => this.picker(false)} />
                <CopyDataFrom showCopyDataPopup={showCopyDataPopup} type={type} backDrop={() => this.setState({ showCopyDataPopup: false })} onPress={(type) => alert(type)} />
                <DateTimePicker
                    isVisible={isDatePicker}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
                {this.state.documentsshow ? <View style={{
                    height: '100%', flex: 1,
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    opacity: 0.7,
                    backgroundColor: 'black',
                    width: '100%',
                    flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
                }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', height: 100, backgroundColor: '#00cc66', position: 'absolute', color: 'black', fontSize: 20 }}>
                        <Text style={{ alignItems: 'center', fontSize: 17, justifyContent: 'center' }}>{'Document Sent \nPayment will be sent upon approval'}</Text>
                        <ButtonC style={{ backgroundColor: '#9D1B34', width: '30%', marginTop: 0 }} onPress={() => navigation.goBack()} title={'ok'} />
                    </View>
                </View> : null}
            </Container>
        )
    }

}
const mapStateToProps = state => ({
    claimData: state.ClaimIntent.claimData,
    driverFront: state.claimApproval.driverFront,
    passengerFront: state.claimApproval.passengerFront,
    driverRear: state.claimApproval.driverRear,
    passengerRear: state.claimApproval.passengerRear,
    claimImages0: state.claimApproval.claimImages0,
    claimImages1: state.claimApproval.claimImages1,
    claimImages2: state.claimApproval.claimImages2,
    claimImages3: state.claimApproval.claimImages3,
    error: state.claimApproval.error,
    isLoading: state.claimApproval.isLoading,
    message: state.claimApproval.message

})

const mapDispatchToProps = {
    getClaim: getClaim
}

export default connect(mapStateToProps, mapDispatchToProps)(claimApproval)