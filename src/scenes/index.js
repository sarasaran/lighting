import LoginMenu from './loginMenu';
import AdminLogin from './adminLogin';
import AdvisorLogin from './advisorLogin';
import Dashboard from './dashboad';
import AdvisorDashboard from './advisorDashboard';
import AdvisorManager from './AdvisorManager';
import AddAdvisor from './addAdvisor';
import Configuration from './configuration';
import ClaimIntent from './claimIntent';
import TireWheelClaim from './tireWheelClaim';
import TireWheelClaimDetails from './tireWheelClaimDetails';
import VINInfo from './vinInformation';
import scanner from './scanner'
import claimApproval from './claimApproval'
export {
    LoginMenu,
    AdminLogin,
    AdvisorLogin,
    AdvisorManager,
    AddAdvisor,
    Dashboard,
    AdvisorDashboard,
    Configuration,
    ClaimIntent,
    TireWheelClaim,
    TireWheelClaimDetails,
    VINInfo,
    scanner,
    claimApproval
}