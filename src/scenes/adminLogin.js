import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';
import { Container, Toast } from 'native-base';
import { adminLoginContainer } from '../styling';
import { ButtonC, CustomInput, AfterLoginHeader3, Loader } from '../components';
import { connect } from 'react-redux';
import { login } from '../store/AdminLogin';
import { getConfigData } from '../store/Configuration';
console.disableYellowBox = true;
class AdminLogin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            key: '02679',
            secret: '343434343'
        }
    }
    

    componentDidMount() {
        const { getConfigData } = this.props;
        getConfigData()
    }

    componentWillReceiveProps(newProps) {
        const { key, secret } = this.state;
        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.isLoggedIn) {
                    this.showToast(newProps.user.data.message)
                    if (newProps.configData.data) {
                        this.props.navigation.navigate('Dashboard', { dealerKey: key, contactid: newProps.configData.data.contactid });
                    } else {
                        this.props.navigation.navigate('Configuration', { key: key, secret: secret });
                    }
                }
            }
        }
    }

    submit = () => {
        const { key, secret } = this.state;
        const { login } = this.props;
        if (key && secret) {
            var body = { "dealerkey": key, "dealersecret": secret }
            login(body)
        } else {
            this.showToast('All fields are required')
        }
    }

    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {
        const { isLoading } = this.props;
        const { key, secret } = this.state
        return (
            <Container >
                <AfterLoginHeader3 title={'Lightning'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View style={adminLoginContainer}>
                    <CustomInput value={key} placeholder={'Dealer Key'} onChangeText={(key) => this.setState({ key })} />
                    <CustomInput value={secret} placeholder={'Dealer Secret'} isSecure={true} onChangeText={(secret) => this.setState({ secret })} />
                    <ButtonC onPress={this.submit} title={'Next'} />
                </View>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    isLoggedIn: state.AdminLogin.isLoggedIn,
    error: state.AdminLogin.error,
    user: state.AdminLogin.user,
    isLoading: state.AdminLogin.isLoading,
    configData: state.Configuration.configData
})

const mapDispatchToProps = {
    login: login,
    getConfigData: getConfigData,
}


export default connect(mapStateToProps, mapDispatchToProps)(AdminLogin)