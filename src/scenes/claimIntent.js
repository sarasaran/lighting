import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import { Container, Content } from 'native-base';
import { dWidth } from '../styling';
import isTablet from '../utility/deviceType';
import { normalize } from '../utility/ResponssiveFont';
import { AfterLoginHeader2, Loader, ClaimIntentTopView } from '../components';
import { connect } from 'react-redux';
import { getClaimIntent } from '../store/ClaimIntent';
import { checked, unchecked } from '../assets';
import { getClaim } from '../store/claimApproval';

const userInfoDiv = { flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center' }
const cardText = { fontSize: isTablet ? 26 : 18, color: '#FFFFFF', textAlign: 'center' }
const cardDiv = { marginBottom: 20, backgroundColor: '#535353', borderRadius: 10, width: isTablet ? dWidth / 3 - 45 : dWidth / 2 - 40, height: isTablet ? normalize(50) : dWidth / 2 - 80, alignItems: 'center', justifyContent: 'center', padding: 20 }
const claimsHeader = { textAlign: 'center', color: '#FFFFFF', fontSize: isTablet ? 26 : 18, padding: isTablet ? 10 : 5, backgroundColor: '#5F9AB0' }
const claimsRow = { flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center', padding: isTablet ? 15 : 5, marginTop: isTablet ? 20 : 10 }
const claimsRowText = { fontSize: isTablet ? 26 : 18, color: '#FFFFFF', textAlign: 'left' }
const claimCheckBox = { tintColor: '#fff', width: isTablet ? 32 : 20, height: isTablet ? 32 : 20, marginRight: isTablet ? 20 : 10 }

class ClaimIntent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            contractid: null
        }
        this.optionclaim = this.optionclaim.bind(this)
    }

    componentDidMount() {
        const { getClaimIntent } = this.props;
        const { params } = this.props.navigation.state;
        const contractid = params ? params.contractid : null;
      
        if (contractid) {
            this.setState({ contractid })
            getClaimIntent(contractid)
        }
    }

    _renderClaims = (data) => {
        if (data) {
            return data.map((v, i) => {
                return (
                    <View key={'claims' + i} style={[claimsRow, { backgroundColor: v.bgcolor }]}>
                        <TouchableOpacity key={'claims' + i} onPress={() => this.optionclaim(v)}>
                            <View>
                                <Text style={claimsRowText}>{'Claim Type'}</Text>
                                <Text style={claimsRowText}>{v.claimtype}</Text>
                            </View>
                            <View>
                                <Text style={claimsRowText}>{'Date Opened'}</Text>
                                <Text style={claimsRowText}>{v.dateopened}</Text>
                            </View>
                            <View>
                                <Text style={claimsRowText}>{'Claim Number'}</Text>
                                <Text style={claimsRowText}>{v.claimnumber}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', padding: isTablet ? 0 : 10, minWidth: 100 }}>
                                <Image source={v.status == "paid" ? checked : unchecked} style={claimCheckBox} />
                                <Text style={claimsRowText}>{v.status}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            })
        } else {
            return null
        }

    }

    _renderOptions = (data) => {
        if (data) {
            return data.map((v, i) => {
                return (
                    <TouchableOpacity key={'options' + i} style={cardDiv} onPress={() => this.optionClick(v)}>
                        <Text style={cardText}>{v}</Text>
                    </TouchableOpacity>
                )
            })
        } else {
            return null
        }

    }
    optionclaim(claimsdetails) {
       
        const { navigation } = this.props;
        if (claimsdetails.status == "paid")
            return;
        else
        {
            navigation.navigate('claimApproval', { claimsdetails: claimsdetails.claimid })
            // var body = {claimid: 'a0A0v0000015zTA' }
            // getClaim(body);
        }
        
            //navigation.navigate('claimApproval', { claimsdetails: claimsdetails.claimid })
    }

    optionClick(option) {
        const { navigation } = this.props;
        const { contractid } = this.state;
        switch (option) {
            case 'Tire / Wheel':
                navigation.navigate('TireWheelClaim', { contractid: contractid })
                break;
            case 'Pre-Paid Maintenance':
                break;
            case 'Key replacement':
                break;
            case 'Appearance':
                break;
            case 'Windshield':
                break;
            case 'PDR':
                break;
            default:
                break;
        }
    }

    render() {

        const { claimData, isLoading } = this.props;
        const data = claimData ? claimData.data : null;
        const city = data ? data.city : null
        const claims = data ? data.claims : null
        const customername = data ? data.customername : null
        const options = data ? data.options : null
        const phone = data ? data.phone : null
        const state = data ? data.state : null
        const street = data ? data.street : null
        const vehicleInfo = data ? data.vehicleInfo : null
        const zipcode = data ? data.zipcode : null

        return (
            <Container >
                <AfterLoginHeader2 name={'Chris Henderson'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <Content>
                    <View style={{ flex: 1 }}>
                        <ClaimIntentTopView customername={customername} street={street} city={city} state={state} zipcode={zipcode} phone={phone} userInfoDiv={userInfoDiv} vehicleInfo={vehicleInfo} />
                        <View style={[{ padding: 20 }, userInfoDiv]}>
                            {this._renderOptions(options)}
                        </View>
                        <Text style={claimsHeader}>Claims</Text>
                        {this._renderClaims(claims)}
                    </View>
                </Content>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: state.ClaimIntent.error,
    isLoading: state.ClaimIntent.isLoading,
    claimData: state.ClaimIntent.claimData
})

const mapDispatchToProps = {
    getClaimIntent: getClaimIntent,
    getClaim: getClaim
}

export default connect(mapStateToProps, mapDispatchToProps)(ClaimIntent)