import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Container, Toast } from 'native-base';
import { advisorManagerContainer, advisorManagerTopText } from '../styling';
import {
    ButtonC, AfterLoginHeader3, AdvisorManagerView, Loader
} from '../components';
import { connect } from 'react-redux';
import { getAdvisors } from '../store/Dashboard';

class AdvisorManager extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }

        this.reload = this.reload.bind(this)
    }


    componentWillMount() {
        this.reload(true)
    }

    submit = () => {

    }

    reload(isChanged) {
        if (isChanged) {
            const { getAdvisors, configData } = this.props;
            getAdvisors(configData.data.key)
        }
    }

    addAdvisor = () => {
        this.props.navigation.navigate('AddAdvisor', { "onUpdate": this.reload })
    }

    viewAdvisor = (index) => {
        this.props.navigation.navigate('AddAdvisor', { advisor: index, "onUpdate": this.reload })
    }

    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {

        const { advisors, navigation, isLoading } = this.props;

        return (
            <Container >
                <AfterLoginHeader3 title={'Lightning Configuration'} onPress={() => navigation.goBack()} isBack={false} />
                <Text style={advisorManagerTopText}>Advisor Management</Text>
                <View style={advisorManagerContainer}>
                    <View style={{ height: '90%', width: '100%' }}>
                        <AdvisorManagerView dataSource={advisors} onPressAdd={this.addAdvisor} onPressAdvisor={(index) => this.viewAdvisor(index)} />
                    </View>
                    <View style={{ height: '10%', width: '100%', alignItems: 'center' }}>
                        <ButtonC style={{ width: '65%' }} onPress={() => navigation.goBack()} title={'Next'} />
                    </View>
                </View>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    advisors: state.Dashboard.advisors,
    isLoading: state.Dashboard.isLoading,
    configData: state.Configuration.configData
})

const mapDispatchToProps = {
    getAdvisors: getAdvisors
}


export default connect(mapStateToProps, mapDispatchToProps)(AdvisorManager)