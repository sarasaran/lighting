import React, { Component } from 'react';
import { View, Text, Picker ,TouchableOpacity,Platform} from 'react-native';
import { Container, Toast, Content,Icon,Title } from 'native-base';
import { adminLoginContainer, loginMenuHeaderText } from '../styling';
import { ButtonC, CustomInput, AfterLoginHeader2 } from '../components';
import isTablet from '../utility/deviceType';
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
import { connect } from 'react-redux';
// import {  getVINInfo } from '../store/VinInfo';

class VINInfo extends Component {

    constructor(props) {
        super(props)
        this.state = {
            vin:'',
            
        }
        
        
    
    }

    // componentDidMount() {
    //     const { params } = this.props.navigation.state;
    //     if (params) {
    //         this.setState({
    //             vin: params.vin,
                
    //         })
    //     }
    // }

    componentWillReceiveProps(newProps) {
        
        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.vinData) {
                   
                    // this.props.navigation.navigate('Dashboard');
                }
            }
        }
    }

    submit = () => {
        const { vin } = this.state;
        const { register } = this.props;
        if (vin) {
            var body = { "vin": vin}
            register(body)
        } else {
            this.showToast('All fields are required')
        }
    }
    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {
        const { vin,key,secret } = this.state
        return (
            <Container >
               
                <AfterLoginHeader2 name={'Chris Henderson'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View style={adminLoginContainer}>
                    <CustomInput value={vin} placeholder={'VIN Number'} onChangeText={(vin) => this.setState({ vin })} />
                    
                    <ButtonC onPress={this.submit} title={'Search'} />
                </View>
                
            </Container>


/* <Container >
    <View style={{flexDirection:'row'}}>
                <AfterLoginHeader3 onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View>
                 <Title style={{ color: '#47525E', fontSize: isTablet ? 26 : 18, fontWeight: '400' }}>SARANYA</Title>
                 <Title style={{ color: '#47525E', fontSize: isTablet ? 26 : 18 }}>Service Advisor</Title>
                 </View>
                </View>
                <View style={adminLoginContainer}>
                    <CustomInput value={vin} placeholder={'VIN Number'} onChangeText={(key) => this.setState({ vin })} />
                    
                    <ButtonC onPress={this.submit} title={'Search'} />
                </View>
                
            </Container>  */
        )
    }
}

const mapStateToProps = state => ({
    error: state.VINInfo.error,
    isLoading: state.VINInfo.isLoading,
    vinData: state.VINInfo.vinData
})




export default VINInfo