import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, TextInput, Alert } from 'react-native';
import { Container, Content, Toast } from 'native-base';
import { dWidth, fontSize } from '../styling';
import isTablet from '../utility/deviceType';
import { AfterLoginHeader2, Loader, ClaimIntentTopView, ButtonC, AddDetailsCard, ConfirmClaimSubmission } from '../components';
import { connect } from 'react-redux';
import { calendar, carTopView } from '../assets';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { formatDate } from '../utility/date';
import { submitClaim, getActive, removeDriveFrontData, removeDriveRearData, removePassangerFrontData, removePassangerRearData } from '../store/TireWheelClaim';

const userInfoDiv = { flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center' }
const highlightedText = { height: isTablet ? 62 : 40, fontSize: isTablet ? 26 : 18, textAlign: 'center', backgroundColor: '#535353', color: '#FFFFFF', padding: isTablet ? 17 : 6 }
const multilineText = { minHeight: isTablet ? 100 : 60, width: isTablet ? '48%' : '100%', borderColor: '#8492A6', borderWidth: 0.5, borderRadius: 2.5, marginBottom: isTablet ? 0 : 5, fontSize: fontSize, padding: isTablet ? 10 : 5, textAlignVertical: 'top' }
const bottomDiv = { zIndex: 100, height: isTablet ? 80 : 60, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor: '#535353', paddingHorizontal: isTablet ? 20 : 10, position: 'absolute', left: 0, right: 0, bottom: 0 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }

class TireWheelClaim extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isSubmitConfirmShow: false,
            dateofDamage: 'Date of Damage/Loss',
            isDatePicker: false,
            contractid: "contractid",
            damagedescription: "",
            generalnotes: "",
            contractid: null
        }

        this._showDateTimePicker = this._showDateTimePicker.bind(this)
        this._hideDateTimePicker = this._hideDateTimePicker.bind(this)
        this._handleDatePicked = this._handleDatePicked.bind(this)
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        const contractid = params ? params.contractid : null;
        if (contractid) {
            this.setState({ contractid })
        }
    }

    componentWillReceiveProps(newProps) {
        const { navigation } = this.props;
        
        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.message) {
                    this.showToast(newProps.message.message)
                    navigation.goBack()
                }
            }
        }
    }

    _showDateTimePicker() {
        this.setState({ isDatePicker: true });
    }

    _hideDateTimePicker() {
        this.setState({ isDatePicker: false });
    }

    _handleDatePicked = (date) => {
        this.setState({
            dateofDamage: formatDate(date)
        })
        this._hideDateTimePicker();
    };

    _cardClick = (side, data, claimImages) => {
        const { navigation, getActive } = this.props;
        getActive(side)
        navigation.navigate('TireWheelClaimDetails', { claimData: data, claimImages: claimImages })
    }

    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    validateAll() {
        const { dateofDamage, contractid, damagedescription, generalnotes } = this.state;
        var issuesArr = this.getIssuesData('all')
        if (dateofDamage && contractid && damagedescription && generalnotes) {
            if (issuesArr.length != 0) {
                this.setState({ isSubmitConfirmShow: true })
            } else {
                this.showToast("No Tire/Wheel issues added.")
            }
        } else {
            this.showToast("All fields are required.")
        }
    }

    actionSubmit() {
        this.setState({ isSubmitConfirmShow: false })
        const { dateofDamage, contractid, damagedescription, generalnotes } = this.state;
        const { submitClaim } = this.props;
        var issuesArr = this.createFinalJSON()
        var body = {
            dateofDamage,
            damagedescription,
            generalnotes,
            contractid,
            claimissues: issuesArr
        }
        
        submitClaim(body)
    }

    createFinalJSON() {
        const { driverFront, passengerFront, driverRear, passengerRear, claimImages0, claimImages1, claimImages2, claimImages3 } = this.props;
        var finalArr = []
        if (driverFront) {
            driverFront.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    v.attachments = this.createAttachmentArr(claimImages0)
                    finalArr.push(v)
                } else {
                    v.attachments = this.createAttachmentArr(claimImages0)
                    finalArr.push(v)
                }
            })
        }
        if (passengerFront) {
            passengerFront.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    v.attachments = this.createAttachmentArr(claimImages1)
                    finalArr.push(v)
                } else {
                    v.attachments = this.createAttachmentArr(claimImages1)
                    finalArr.push(v)
                }
            })
        }
        if (driverRear) {
            driverRear.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    v.attachments = this.createAttachmentArr(claimImages2)
                    finalArr.push(v)
                } else {
                    v.attachments = this.createAttachmentArr(claimImages2)
                    finalArr.push(v)
                }
            })
        }
        if (passengerRear) {
            passengerRear.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    v.attachments = this.createAttachmentArr(claimImages3)
                    finalArr.push(v)
                } else {
                    v.attachments = this.createAttachmentArr(claimImages3)
                    finalArr.push(v)
                }
            })
        }
        return finalArr
    }

    createAttachmentArr(image) {
        if (image) {
            const { closeupViewImageData, fullViewImageData, straightOnViewImageData } = image;
            if (closeupViewImageData && fullViewImageData && straightOnViewImageData) {
                var arr = [
                    {
                        "file": fullViewImageData.data,
                        "attachmentname": "FullView"
                    }, {
                        "file": closeupViewImageData.data,
                        "attachmentname": "Close-UpView"
                    }, {
                        "file": straightOnViewImageData.data,
                        "attachmentname": "Straight-OnView"
                    }
                ]
                return arr;
            } else {
                return null
            }
        }
        return null
    }

    getIssuesData(type) {
        const { driverFront, passengerFront, driverRear, passengerRear, claimImages0, claimImages1, claimImages2, claimImages3 } = this.props;
        var tireIssuesArr = []
        var wheelIssuesArr = []
        var allIssuesArr = []
        var priceTotal = 0
        if (driverFront) {
            driverFront.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    wheelIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                } else {
                    tireIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                }
            })
        }
        if (passengerFront) {
            passengerFront.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    wheelIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                } else {
                    tireIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                }
            })
        }
        if (driverRear) {
            driverRear.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    wheelIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                } else {
                    tireIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                }
            })
        }
        if (passengerRear) {
            passengerRear.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    wheelIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                } else {
                    tireIssuesArr.push(v)
                    priceTotal = priceTotal + parseFloat(v.partscost) + parseFloat(v.laborcost) + parseFloat(v.othercost) + parseFloat(v.tax)
                }
            })
        }
        allIssuesArr = tireIssuesArr.concat(wheelIssuesArr)
        switch (type) {
            case 'wheel':
                return wheelIssuesArr.length;
            case 'tire':
                return tireIssuesArr.length;
            case 'all':
                return allIssuesArr;
            case 'price':
                return priceTotal;
            default:
                break;
        }

    }

    removeClaim(index) {
        Alert.alert(
            'Alert',
            'are you sure you wish to remove?',
            [
                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'Yes', onPress: () => this.deletionConfirmed(index) }
            ]
        )
    }

    deletionConfirmed(index) {
        const { removeDriveFrontData, removeDriveRearData, removePassangerFrontData, removePassangerRearData } = this.props;
        switch (index) {
            case 0:
                removeDriveFrontData();
                break;
            case 1:
                removePassangerFrontData();
                break;
            case 2:
                removeDriveRearData();
                break;
            case 3:
                removePassangerRearData();
                break;
            default:
                break;
        }
    }


    render() {
        const { claimData, isLoading, navigation, driverFront, passengerFront, driverRear, passengerRear,
            claimImages0, claimImages1, claimImages2, claimImages3
        } = this.props;
        const { dateofDamage, isDatePicker, damagedescription, generalnotes, isSubmitConfirmShow } = this.state;
        const data = claimData ? claimData.data : null;
        const city = data ? data.city : null
        const customername = data ? data.customername : null
        const phone = data ? data.phone : null
        const state = data ? data.state : null
        const street = data ? data.street : null
        const vehicleInfo = data ? data.vehicleInfo : null
        const zipcode = data ? data.zipcode : null

        return (
            <Container >
                <AfterLoginHeader2 name={'Chris Henderson'} onPress={() => navigation.goBack()} isBack={true} />
                <Content>
                    <View style={{ flex: 1 }}>
                        <ClaimIntentTopView customername={customername} street={street} city={city} state={state} zipcode={zipcode} phone={phone} userInfoDiv={userInfoDiv} vehicleInfo={vehicleInfo} />
                        <Text style={highlightedText}>Tire/Wheel Claim</Text>
                        <View style={{ width: isTablet ? '40%' : '60%', marginTop: 15, marginLeft: isTablet ? 20 : 8, padding: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderColor: '#47525E', borderWidth: 0.5, borderRadius: 2.5 }}>
                            <Text style={{ width: '80%', color: '#47525E', fontSize: isTablet ? 22 : 16, marginRight: 15 }}>{dateofDamage}</Text>
                            <TouchableOpacity onPress={() => this.setState({ isDatePicker: true })}>
                                <Image source={calendar} style={{ width: isTablet ? 36 : 24, height: isTablet ? 36 : 24 }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', padding: isTablet ? 20 : 8 }}>
                            <TextInput
                                value={damagedescription}
                                onChangeText={(damagedescription) => this.setState({ damagedescription })}
                                multiline={true}
                                maxLength={500}
                                placeholder={'Describe how the damage/loss occurred'}
                                style={multilineText}
                                underlineColorAndroid="transparent" />
                            <TextInput
                                value={generalnotes}
                                onChangeText={(generalnotes) => this.setState({ generalnotes })}
                                multiline={true}
                                maxLength={500}
                                placeholder={'General Notes'}
                                style={multilineText}
                                underlineColorAndroid="transparent" />
                        </View>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}>
                            <View style={{ width: '100%', height: '100%', position: 'absolute', left: 0, top: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={carTopView} style={{ width: '50%', height: '75%' }} resizeMode='contain' />
                                <View style={{ width: '50%', height: 0.5, position: 'absolute', left: '25%', top: '50%', backgroundColor: 'black' }}></View>
                                <View style={{ width: 0.5, height: '90%', position: 'absolute', left: '50%', top: '5%', backgroundColor: 'black' }}></View>
                            </View>
                            <AddDetailsCard onPress={() => this._cardClick(0, driverFront, claimImages0)} removePress={() => this.removeClaim(0)} align={'left'} title={'Driver Front'} data={driverFront} style={{ marginBottom: isTablet ? 40 : 20 }} />
                            <AddDetailsCard onPress={() => this._cardClick(1, passengerFront, claimImages1)} removePress={() => this.removeClaim(1)} align={'right'} title={'Passenger Front'} data={passengerFront} style={{ marginBottom: isTablet ? 40 : 20 }} />
                            <AddDetailsCard onPress={() => this._cardClick(2, driverRear, claimImages2)} removePress={() => this.removeClaim(2)} align={'left'} title={'Driver Rear'} data={driverRear} />
                            <AddDetailsCard onPress={() => this._cardClick(3, passengerRear, claimImages3)} removePress={() => this.removeClaim(3)} align={'right'} title={'Passenger Rear'} data={passengerRear} />
                        </View>
                        <View style={{ width: isTablet ? dWidth - 40 : dWidth - 20, marginLeft: isTablet ? 20 : 10, borderTopColor: '#B1B1B1', borderTopWidth: isTablet ? 4 : 2, marginTop: isTablet ? 20 : 10 }}></View>
                        <View style={{ padding: isTablet ? 20 : 10, marginBottom: isTablet ? 90 : 70 }}>
                            <Text style={smallText}>Tire Issues:  {this.getIssuesData('tire')}</Text>
                            <Text style={smallText}>Wheel Issues:  {this.getIssuesData('wheel')}</Text>
                            <Text style={smallText}>Total Requested: ${this.getIssuesData('price')}</Text>
                        </View>
                    </View>
                </Content>
                <View style={bottomDiv}>
                    <ButtonC style={{ backgroundColor: '#9D1B34', width: '40%', marginTop: 0 }} onPress={() => navigation.goBack()} title={'CANCEL'} />
                    <ButtonC style={{ backgroundColor: '#19831C', width: '40%', marginTop: 0 }} onPress={() => this.validateAll()} title={'Submit Claim'} />
                </View>
                <Loader isLoading={isLoading} />
                <ConfirmClaimSubmission visible={isSubmitConfirmShow} backDrop={() => this.setState({ isSubmitConfirmShow: false })} cancel={() => this.setState({ isSubmitConfirmShow: false })} submit={() => this.actionSubmit()} />
                <DateTimePicker
                    isVisible={isDatePicker}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    claimData: state.ClaimIntent.claimData,
    activeClaim: state.TireWheelClaim.activeClaim,
    driverFront: state.TireWheelClaim.driverFront,
    passengerFront: state.TireWheelClaim.passengerFront,
    driverRear: state.TireWheelClaim.driverRear,
    passengerRear: state.TireWheelClaim.passengerRear,
    claimImages0: state.TireWheelClaim.claimImages0,
    claimImages1: state.TireWheelClaim.claimImages1,
    claimImages2: state.TireWheelClaim.claimImages2,
    claimImages3: state.TireWheelClaim.claimImages3,
    error: state.TireWheelClaim.error,
    isLoading: state.TireWheelClaim.isLoading,
    message: state.TireWheelClaim.message

})

const mapDispatchToProps = {
    getActive: getActive,
    removeDriveFrontData: removeDriveFrontData,
    removeDriveRearData: removeDriveRearData,
    removePassangerFrontData: removePassangerFrontData,
    removePassangerRearData: removePassangerRearData,
    submitClaim: submitClaim
}

export default connect(mapStateToProps, mapDispatchToProps)(TireWheelClaim)