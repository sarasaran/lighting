import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Toast } from 'native-base';
import { adminLoginContainer } from '../styling';
import { ButtonC, CustomInput, AfterLoginHeader3, Loader } from '../components';
import { connect } from 'react-redux';
import { register } from '../store/Configuration';

class AdvisorLogin extends Component {

    constructor(props) {
        super(props)
        this.state = {
            key: '',
            name: '',
            pin: ''
        }
    }

    componentWillReceiveProps(newProps) {
        const { key } = this.state
        const { saveConfigurationLocal } = this.props;
        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.actionData) {
                    this.showToast(newProps.actionData.data.message)
                    this.props.navigation.navigate('AdvisorDashboard', { dealerKey: key });
                }
            }
        }
    }

    submit = () => {
        const { key, name, pin } = this.state
        const { register } = this.props;
        if (key && name && pin) {
            var body = { "dealerkey": key, "name": name, "password": pin }
            register(body)
        } else {
            this.showToast('All fields are required')
        }

    }
    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {
        const { key, pin, name } = this.state;
        const { isLoading } = this.props;
        return (
            <Container >
                <AfterLoginHeader3 title={'Auto-Login Advisor'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View style={adminLoginContainer}>
                    <CustomInput value={key} placeholder={'Dealer Key'} onChangeText={(key) => this.setState({ key })} />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '80%' }}>
                        <CustomInput value={name} placeholder={'Advisor Name'} onChangeText={(name) => this.setState({ name })} style={{ width: '58%' }} />
                        <CustomInput value={pin} placeholder={'Advisor PIN'} isSecure={true} onChangeText={(pin) => this.setState({ pin })} style={{ width: '40%' }} />
                    </View>
                    <ButtonC style={{ width: '65%' }} onPress={this.submit} title={'Confirm Auto-Login'} />
                </View>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: state.Configuration.error,
    isLoading: state.Configuration.isLoading,
    actionData: state.Configuration.actionData
})

const mapDispatchToProps = {
    register: register
}


export default connect(mapStateToProps, mapDispatchToProps)(AdvisorLogin)