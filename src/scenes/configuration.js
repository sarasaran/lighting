import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { Container, Toast, Content } from 'native-base';
import { adminLoginContainer, loginMenuHeaderText } from '../styling';
import { ButtonC, CustomInputWithTitle, AfterLoginHeader3, CustomPicker } from '../components';
import isTablet from '../utility/deviceType';
import { connect } from 'react-redux';
import { register, saveConfigurationLocal } from '../store/Configuration';

class Configuration extends Component {

    constructor(props) {
        super(props)
        this.state = {
            key: '02679',
            secret: '343434343',
            email: 'email@gmail.com',
            password: 'hh@123',
            fname: 'gg',
            lname: 'hh',
            phone: '1234567890',
            contactid: '123',
            title: 'Service Director'
        }
        
        
    
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        if (params) {
            this.setState({
                key: params.key,
                secret: params.secret
            })
        }
    }

    componentWillReceiveProps(newProps) {
        const { saveConfigurationLocal } = this.props;
        if (newProps != this.props) {
            if (newProps.error) {
                this.showToast(newProps.error.error.message)
            } else {
                if (newProps.actionData) {
                    this.state.contactid = newProps.actionData.data.contactid;
                    this.setState({
                        contactid: newProps.actionData.data.contactid
                    })
                    this.showToast(newProps.actionData.data.message);
                   
                    saveConfigurationLocal(this.state)
                    // this.props.navigation.navigate('Dashboard');
                }
            }
        }
    }

    submit = () => {
        const { key, secret, email, password, fname, lname, phone, title } = this.state;
        const { register } = this.props;
        if (key && secret && email && password && fname && lname && phone && title) {
            var body = { "dealerkey": key, "dealersecret": secret, "email": email, "firstname": fname, "lastname": lname, "phone": phone, "password": password, "title": title }
            register(body)
        } else {
            this.showToast('All fields are required')
        }
    }
    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {
        const { key, secret, email, password, fname, lname, phone, title } = this.state
        return (
            <Container >
                <AfterLoginHeader3 title={'Lightning Configuration'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View style={[adminLoginContainer, { alignItems: isTablet ? 'center' : 'flex-start', padding: 0 }]}>
                    <Content>
                        <Text style={loginMenuHeaderText}>{'Initial Setup'}</Text>
                        <View style={{ width: '100%', alignItems: 'center', padding: 20 }}>
                            <View style={{ width: isTablet ? '70%' : '100%', alignItems: 'center' }}>
                                <CustomInputWithTitle title="Dealer Id" value={key} placeholder={'Dealer Id'} onChangeText={(key) => this.setState({ key })} />
                                <CustomInputWithTitle title="Dealer Key" value={secret} placeholder={'Dealer Key'} onChangeText={(secret) => this.setState({ secret })} isSecure={true} />
                                <CustomInputWithTitle title="Email Address" value={email} placeholder={'Email Address'} onChangeText={(email) => this.setState({ email })} />
                                <CustomInputWithTitle title="First Name" value={fname} placeholder={'First Name'} onChangeText={(fname) => this.setState({ fname })} />
                                <CustomInputWithTitle title="Last Name" value={lname} placeholder={'Last Name'} onChangeText={(lname) => this.setState({ lname })} />
                                <CustomInputWithTitle title="Phone" value={phone} placeholder={'Phone'} onChangeText={(phone) => this.setState({ phone })} />
                                <CustomPicker title={'Title'} value={title} onValueChange={(v, i) => this.setState({ title: v })} />
                                <CustomInputWithTitle title="PIN" value={password} placeholder={'PIN'} isSecure={true} onChangeText={(password) => this.setState({ password })} />
                                <ButtonC style={{ width: isTablet ? '100%' : '90%' }} onPress={this.submit} title={'Save'} />
                            </View>
                        </View>
                    </Content>
                </View>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: state.Configuration.error,
    isLoading: state.Configuration.isLoading,
    actionData: state.Configuration.actionData
})

const mapDispatchToProps = {
    register: register,
    saveConfigurationLocal: saveConfigurationLocal
}


export default connect(mapStateToProps, mapDispatchToProps)(Configuration)