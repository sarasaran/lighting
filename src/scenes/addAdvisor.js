import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image, Alert } from 'react-native';
import { Container, Toast } from 'native-base';
import { addAdvisorContainer, addAdvisorImageView } from '../styling';
import isTablet from '../utility/deviceType';
import {
    ButtonC, CustomInputWithTitle, AfterLoginHeader3, Loader
} from '../components';
import { connect } from 'react-redux';
import { addAdvisor, updateAdvisor, deleteAdvisor, isAdvisorRemovable } from '../store/Dashboard';

class AddAdvisor extends Component {

    constructor(props) {
        super(props)
        this.state = {
            advisor: null,
            name: '',
            title: '',
            pin: ''
        }
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        const advisor = params ? params.advisor : null
        if (advisor) {
            this.setState({
                advisor: advisor,
                name: advisor.advisorname,
                pin: advisor.password,
                title: advisor.title
            })
        }
    }

    componentWillReceiveProps(newProps) {
        console.log('props', newProps)
        if (newProps != this.props) {
            if (newProps.error) {
                console.log(newProps.error)
            } else {
                if (newProps.message) {
                    this.showToast(newProps.message.data.message)
                    this.goBack()
                }
                if (newProps.isRemovable) {
                    if (newProps.isRemovable.data.removable) {
                        Alert.alert(
                            'Alert',
                            'are you sure you wish to delete user',
                            [
                                { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                                { text: 'Yes', onPress: () => this.confirmedDelete() },
                            ]
                        )
                    } else {
                        Alert.alert(
                            'Alert',
                            'cannot delete user, user has open work',
                            [
                                { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' }
                            ]
                        )
                    }
                }
            }
        }
    }

    goBack() {
        const { navigation } = this.props;
        navigation.state.params.onUpdate(true);
        navigation.goBack();
    }

    submit = () => {
        const { name, title, pin, advisor } = this.state;
        const { addAdvisor, configData, updateAdvisor } = this.props;
        if (advisor) {
            var body = { "name": name, "password": pin, "title": title, "contactid": advisor.advisorid, "dealerkey": configData.data.key }
            updateAdvisor(body)
        } else {
            var body = { "name": name, "password": pin, "title": title, "dealerkey": configData.data.key }
            addAdvisor(body);
        }
    }

    deleteAdvisor = () => {
        const { advisor } = this.state;
        const { isAdvisorRemovable } = this.props;
        isAdvisorRemovable(advisor.advisorid)
    }

    confirmedDelete = () => {
        const { advisor } = this.state;
        const { deleteAdvisor } = this.props;
        var body = { "contactid": advisor.advisorid }
        deleteAdvisor(body)
    }

    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    render() {

        const { name, title, pin, advisor } = this.state;
        const { isLoading } = this.props;

        return (
            <Container >
                <AfterLoginHeader3 title={'Advisor Management'} onPress={() => this.props.navigation.goBack()} isBack={true} />
                <View style={[addAdvisorContainer]}>
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity style={addAdvisorImageView}>
                            <Image source={require('../assets/user.png')} style={{ width: '100%', height: '100%', opacity: 0.35 }} />
                        </TouchableOpacity>
                        <View style={{ width: isTablet ? '50%' : '80%' }}>
                            <CustomInputWithTitle title={'Name'} value={name} placeholder={'Fullname'} onChangeText={(name) => this.setState({ name })} />
                            <CustomInputWithTitle title={'Title'} value={title} placeholder={'Title'} onChangeText={(title) => this.setState({ title })} />
                            <CustomInputWithTitle keyboardType={'number-pad'} maxLength={4} isSecure={true} title={'PIN'} value={pin} placeholder={'PIN'} onChangeText={(pin) => this.setState({ pin })} />
                            {isTablet && <ButtonC style={{ width: '100%' }} onPress={this.submit} title={'Save'} />}
                        </View>
                        {isTablet && advisor &&
                            <View style={{ width: '20%', alignItems: 'center', justifyContent: 'flex-start', marginLeft: 80 }}>
                                <ButtonC style={{ width: '80%', backgroundColor: 'red' }} onPress={this.deleteAdvisor} title={'Delete'} />
                            </View>
                        }
                    </View>
                    {!isTablet && <View style={{ alignItems: 'center', width: '100%' }}>
                        <ButtonC style={{ width: isTablet ? '50%' : '90%' }} onPress={this.submit} title={'Save'} />
                        {advisor && <ButtonC style={{ width: '90%', backgroundColor: 'red' }} onPress={this.deleteAdvisor} title={'Delete'} />}
                    </View>}
                </View>
                <Loader isLoading={isLoading} />
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    error: state.Dashboard.error,
    isLoading: state.Dashboard.isLoading,
    dashData: state.Dashboard.dashData,
    configData: state.Configuration.configData,
    message: state.Dashboard.message,
    isRemovable: state.Dashboard.isRemovable
})

const mapDispatchToProps = {
    addAdvisor: addAdvisor,
    updateAdvisor: updateAdvisor,
    deleteAdvisor: deleteAdvisor,
    isAdvisorRemovable: isAdvisorRemovable
}


export default connect(mapStateToProps, mapDispatchToProps)(AddAdvisor)