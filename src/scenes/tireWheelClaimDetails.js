import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, ScrollView,Image } from 'react-native';
import { Container, Content, Form, Toast } from 'native-base';
import { fontSize } from '../styling';
import { Loader, ButtonC, CardImage, TireClaim, WheelClaim, ImagePickerPopup, ImagePreview, CopyDataFrom } from '../components';
import isTablet from '../utility/deviceType';
import { image1, image2, image3 } from '../assets';
import { connect } from 'react-redux';
// import ImagePicker from 'react-native-image-crop-picker';
import * as types from '../store/TireClaim/actionTypes';
import * as types2 from '../store/WheelClaim/actionTypes';
import { createPassangerRearData, createDriveFrontData, createDriveRearData, createPassangerFrontData } from '../store/TireWheelClaim';
import { ImagePicker,Permissions } from 'expo';

const highlightedText = { height: isTablet ? 62 : 40, fontSize: isTablet ? 26 : 18, textAlign: 'center', backgroundColor: '#535353', color: '#FFFFFF', padding: isTablet ? 17 : 6 }
const bottomDiv = { zIndex: 100, height: isTablet ? 80 : 60, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor: '#535353', paddingHorizontal: isTablet ? 20 : 10, position: 'absolute', left: 0, right: 0, bottom: 0 }

class TireWheelClaimDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            type: 1,
            changeImageIndex: 0,
            showImagePicker: false,
            showImagePreview: false,
            showCopyDataPopup: false,
            activePreviewImage: null,
            fullViewImageData: null,
            closeupViewImageData: null,
            straightOnViewImageData: null,
            isWheelFileClaim: false,
            isTireFileClaim: false,
           
            wheelClaim: {
                issuetype: "Wheel",
                damagerepairable: true,
                damagelocation: "",
                reasonforwheelnonrepairable: "",
                wheeldiameter: "Wheel Diameter",
                treaddepth: "Tread Depth",
                wheelbrand: "",
                wheelmodel: "",
                damagetype: "Type of Damage",
                wheeltype: "Wheel Type",
                damagearea: "Damage Area",
                partnumber: "",
                wheelfinishdescription: "",
                partscost: "",
                laborcost: "",
                othercost: "",
                othercostdescription: "",
                tax: ""
            },
            tireClaim: {
                issuetype: "Tire",
                damagerepairable: true,
                reasonfortirenonrepairable: "",
                damagelocation: "",
                damagetype: "Type of Damage",
                damagearea: "Damage Area",
                tirewidth: "Tire Width",
                tireaspectratio: "Tire Aspect Ratio",
                wheeldiameter: "Wheel Diameter",
                speedrating: "Speed Rating",
                treaddepth: "Tread Depth",
                partnumber: "",
                tirebrand: "Tire Brand",
                tiremodel: "",
                partscost: "",
                laborcost: "",
                othercost: "",
                othercostdescription: "",
                tax: ""
            }
        }

        this.setImage = this.setImage.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onValueChange2 = this.onValueChange2.bind(this);
        this.setClaimLocation = this.setClaimLocation.bind(this);
        this.createJSON = this.createJSON.bind(this);
        this.setClaimDataOnLoad = this.setClaimDataOnLoad.bind(this)
    }

    async componentDidMount() {
        const { activeClaim } = this.props;
        const { params } = this.props.navigation.state;
        const claimData = params ? params.claimData : null
        const claimImages = params ? params.claimImages : null
        this.setClaimLocation(activeClaim)
        this.setClaimDataOnLoad(claimData, claimImages)
        const cameraPermission=await Expo.Permissions.askAsync(Expo.Permissions.CAMERA);
        const cameraRollPermission=await Expo.Permissions.askAsync(Expo.Permissions.CAMERA_ROLL);
    }

    setClaimDataOnLoad(data, image) {
        if (data) {
            data.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    this.setState({ wheelClaim: v, isWheelFileClaim: true })
                } else {
                    this.setState({ tireClaim: v, isTireFileClaim: true })
                }
            })
        }
        
        if (image) {
            const { fullViewImageData, closeupViewImageData, straightOnViewImageData } = image;
            this.setState({ fullViewImageData })
            this.setState({ closeupViewImageData })
            this.setState({ straightOnViewImageData })
        }
       
    }


    setClaimLocation(i) {
        switch (i) {
            case 0:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagelocation: "Driver Front" }, wheelClaim: { ...prevState.wheelClaim, damagelocation: "Driver Front" } }))
                break;
            case 1:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagelocation: "Passanger Front" }, wheelClaim: { ...prevState.wheelClaim, damagelocation: "Passanger Front" } }))
                break;
            case 2:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagelocation: "Driver Rear" }, wheelClaim: { ...prevState.wheelClaim, damagelocation: "Driver Rear" } }))
                break;
            case 3:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagelocation: "Passanger Rear" }, wheelClaim: { ...prevState.wheelClaim, damagelocation: "Passanger Rear" } }))
                break;
            default:
                break;
        }
    }

    _renderImages = (images) => {
        if (images) {
            return images.map((v, i) => {
                return <CardImage key={'cardImage' + i} onPress={() => alert('upload Image')} title={'Full View'} image={image1} />
            })
        } else {
            return null
        }
    }

    async picker(camera) {
        this.setState({ showImagePicker: false })
        if (camera) {
            let captureImage = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4, 6],
                base64: true,
              });
          
              if (!captureImage.cancelled) {
                let Image=captureImage;
                this.setImage(Image);
              } 
        
        
        } else 
        {
        let captureGalleryImage = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 6],
            base64: true,
          });
      
          if (!captureGalleryImage.cancelled) {
             let GalleryImage = captureGalleryImage;
             this.setImage(GalleryImage);
          } 
        }
           
    }

    setImage(image) {
        const { changeImageIndex } = this.state;
       console.log(image);
        switch (changeImageIndex) {
            case 0:
                this.setState({ fullViewImageData: image })
                break;
            case 1:
                this.setState({ closeupViewImageData: image })
                break;
            case 2:
                this.setState({ straightOnViewImageData: image })
                break;
            default:
                break;
        }
    }

    onImageClick(image, index) {

        if (image) {
            this.setState({ activePreviewImage: image, showImagePreview: true })
        } else {
            this.setState({ changeImageIndex: index, showImagePicker: true })
        }
    }

    submitTireWheelClaim() {
        const { isTireFileClaim, isWheelFileClaim, wheelClaim, tireClaim } = this.state;
       
        if (isTireFileClaim && isWheelFileClaim) {
            if (this.validateTire() && this.validateWheel()) {
                this.createJSON([wheelClaim, tireClaim])
            } else {
                this.showToast("All fields are required.")
            }
        } else if (isWheelFileClaim) {
            if (this.validateWheel()) {
                this.createJSON([wheelClaim])
            }
        } else if (isTireFileClaim) {
            if (this.validateTire()) {
                this.createJSON([tireClaim])
            }
        } else {
            this.showToast("Please enable File")
        }
    }

    showToast = (text) => {
        Toast.show({
            text: text,
            buttonText: "Okay",
            position: "top",
            style:{marginTop:22}
        })
    }

    createJSON(data) {
        const { navigation, activeClaim, createDriveFrontData, createDriveRearData, createPassangerFrontData, createPassangerRearData } = this.props;
        const { fullViewImageData, closeupViewImageData, straightOnViewImageData } = this.state;
        const imageData = { fullViewImageData, closeupViewImageData, straightOnViewImageData };
        switch (activeClaim) {
            case 0:
                createDriveFrontData(data, imageData)
                break;
            case 1:
                createPassangerFrontData(data, imageData)
                break;
            case 2:
                createDriveRearData(data, imageData)
                break;
            case 3:
                createPassangerRearData(data, imageData)
                break;
            default:
                break;
        }
        navigation.goBack()
    }

    validateWheel() {
        const { damagerepairable, damagelocation, reasonforwheelnonrepairable, wheeldiameter, treaddepth,
            wheelbrand, wheelmodel, damagetype, wheeltype, damagearea, partnumber, wheelfinishdescription,
            partscost, laborcost, othercost, othercostdescription, tax } = this.state.wheelClaim;
        const { fullViewImageData, closeupViewImageData, straightOnViewImageData } = this.state;
        if (damagearea == "Damage Area" || damagetype == "Type of Damage" || wheeldiameter == "Wheel Diameter" ||
            treaddepth == "Tread Depth" || wheelbrand == "" || wheelmodel == "" || wheeltype == "Wheel Type" ||
            partnumber == "" || wheelfinishdescription == "" || partscost == "" || laborcost == "" || othercost == "" ||
            othercostdescription == "" || tax == "" || !fullViewImageData || !closeupViewImageData || !straightOnViewImageData
        ) {
            this.showToast("All fields are required.")
            return false
        } else {
            if (damagerepairable) {
                return true
            } else {
                if (reasonforwheelnonrepairable != "") {
                    return true
                }
                this.showToast("All fields are required.")
                return false
            }
        }
    }

    validateTire() {
        const { damagerepairable, reasonfortirenonrepairable, damagelocation, damagetype, damagearea, tirewidth,
            tireaspectratio, wheeldiameter, speedrating, treaddepth, partnumber, tirebrand, tiremodel, partscost,
            laborcost, othercost, othercostdescription, tax } = this.state.tireClaim;
        const { fullViewImageData, closeupViewImageData, straightOnViewImageData } = this.state;
        if (damagetype == "Type of Damage" || damagearea == "Damage Area" || tirewidth == "Tire Width" || tireaspectratio == "Tire Aspect Ratio" ||
            wheeldiameter == "Wheel Diameter" && speedrating == "Speed Rating" || treaddepth == "Tread Depth" ||
            partnumber == "" || tirebrand == "Tire Brand" || tiremodel == "" || partscost == "" || laborcost == "" ||
            othercost == "" || othercostdescription == "" || tax == "") {
            this.showToast("All fields are required.")
            return false
        } else {
            if (damagerepairable) {
                return true
            } else {
                if (reasonfortirenonrepairable == "") {
                    this.showToast("Please enter reason why tire is not repairable.");
                    return false
                } else if (!fullViewImageData || !closeupViewImageData || !straightOnViewImageData) {
                    this.showToast("Please add images.")
                    return false
                }
                return true
            }
        }
    }

    onValueChange(data, type) {
        switch (type) {
            case types.DAMAGE_REPAIRABLE_CHANGE:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagerepairable: data } }))
                break;
            case types.REASON_TEXT_CHANGE:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, reasonfortirenonrepairable: data } }))
                break;
            case types.TYPES_OF_DAMAGE:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagetype: data } }))
                break;
            case types.DAMAGE_AREA:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, damagearea: data } }))
                break;
            case types.TIRE_WIDTH:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, tirewidth: data } }))
                break;
            case types.TIRE_ASPECT_RATIO:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, tireaspectratio: data } }))
                break;
            case types.WHEEL_DIAMETER:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, wheeldiameter: data } }))
                break;
            case types.SPEED_RATING:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, speedrating: data } }))
                break;
            case types.TREAD_DEPTH:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, treaddepth: data } }))
                break;
            case types.PART_NUMBER:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, partnumber: data } }))
                break;
            case types.TIRE_BRAND:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, tirebrand: data } }))
                break;
            case types.TIRE_MODEL:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, tiremodel: data } }))
                break;
            case types.PART_COST:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, partscost: data } }))
                break;
            case types.LABOR_COST:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, laborcost: data } }))
                break;
            case types.OTHER_COST:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, othercost: data } }))
                break;
            case types.OTHER_COST_DESCRIPTION:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, othercostdescription: data } }))
                break;
            case types.TAX:
                this.setState(prevState => ({ tireClaim: { ...prevState.tireClaim, tax: data } }))
                break;
            default:
                console.log("")
        }
    }

    onValueChange2(data, type) {
        switch (type) {
            case types2.DAMAGE_REPAIRABLE_CHANGE:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, damagerepairable: data } }))
                break;
            case types2.REASON_TEXT_CHANGE:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, reasonforwheelnonrepairable: data } }))
                break;
            case types2.WHEEL_DIAMETER:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, wheeldiameter: data } }))
                break;
            case types2.TREAD_DEPTH:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, treaddepth: data } }))
                break;
            case types2.WHEEL_BRAND:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, wheelbrand: data } }))
                break;
            case types2.WHEEL_MODEL:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, wheelmodel: data } }))
                break;
            case types2.TYPES_OF_DAMAGE:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, damagetype: data } }))
                break;
            case types2.WHEEL_TYPE:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, wheeltype: data } }))
                break;
            case types2.DAMAGE_AREA:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, damagearea: data } }))
                break;
            case types2.PART_NUMBER:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, partnumber: data } }))
                break;
            case types2.WHEEL_FINISH_DESCRIPTION:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, wheelfinishdescription: data } }))
                break;
            case types2.PART_COST:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, partscost: data } }))
                break;
            case types2.LABOR_COST:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, laborcost: data } }))
                break;
            case types2.OTHER_COST:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, othercost: data } }))
                break;
            case types2.OTHER_COST_DESCRIPTION:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, othercostdescription: data } }))
                break;
            case types2.TAX:
                this.setState(prevState => ({ wheelClaim: { ...prevState.wheelClaim, tax: data } }))
                break;
            default:
                console.log("")
        }
    }


    render() {

        const { isLoading, navigation } = this.props;
        const { showImagePicker, activePreviewImage, showImagePreview, isWheelFileClaim, isTireFileClaim,
            fullViewImageData, closeupViewImageData, straightOnViewImageData, wheelClaim, tireClaim,
            showCopyDataPopup, type
        } = this.state;

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Container >
                    <Content>
                        <Form>
                            <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
                                <TireClaim navigation={navigation} enabled={isTireFileClaim} onToggleFile={isTireFileClaim => this.setState({ isTireFileClaim })} claimData={tireClaim} onValueChange={this.onValueChange} onCopyDataClick={() => this.setState({ showCopyDataPopup: true, type: 1 })} />
                                <WheelClaim navigation={navigation} enabled={isWheelFileClaim} onToggleFile={isWheelFileClaim => this.setState({ isWheelFileClaim })} claimData={wheelClaim} onValueChange={this.onValueChange2} onCopyDataClick={() => this.setState({ showCopyDataPopup: true, type: 2 })} />
                            </View>
                        </Form>
                        <Text style={highlightedText}>Photos</Text>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                            <View style={{ marginBottom: 100, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 15 }}>
                                <CardImage onPress={() => this.onImageClick(fullViewImageData, 0)} title={'Full View'} image={fullViewImageData} />
                                <CardImage onPress={() => this.onImageClick(closeupViewImageData, 1)} title={'Close-Up View'} image={closeupViewImageData} />
                                <CardImage onPress={() => this.onImageClick(straightOnViewImageData, 2)} title={'Straight-On View'} image={straightOnViewImageData} />
                               <ButtonC style={{ backgroundColor: '#47525E', marginTop: 0, borderRadius: 8, width: 120 }} onPress={() => this.setState({ showImagePicker: true })} title={'Add More'} />
                            </View>
                        </ScrollView>
                    </Content>
                    <View style={bottomDiv}>
                        <ButtonC style={{ backgroundColor: '#9D1B34', width: '40%', marginTop: 0 }} onPress={() => navigation.goBack()} title={'CANCEL'} />
                        <ButtonC style={{ backgroundColor: '#19831C', width: '40%', marginTop: 0 }} onPress={() => this.submitTireWheelClaim()} title={'SAVE'} />
                    </View>
                    <Loader isLoading={isLoading} />
                    <ImagePreview showImagePreview={showImagePreview} image={activePreviewImage} cancel={() => this.setState({ showImagePreview: false })} onYes={() => this.setState({ showImagePreview: false, showImagePicker: true })} backDrop={() => this.setState({ showImagePreview: false })} />
                    <ImagePickerPopup showImagePicker={showImagePicker} backDrop={() => this.setState({ showImagePicker: false })} close={() => this.setState({ showImagePicker: false })} cameraClick={() => this.picker(true)} galleryClick={() => this.picker(false)} />
                    <CopyDataFrom showCopyDataPopup={showCopyDataPopup} type={type} backDrop={() => this.setState({ showCopyDataPopup: false })} onPress={(type) => alert(type)} />
                </Container>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => ({
    activeClaim: state.TireWheelClaim.activeClaim,
    driverFront: state.TireWheelClaim.driverFront,
    passengerFront: state.TireWheelClaim.passengerFront,
    driverRear: state.TireWheelClaim.driverRear,
    passengerRear: state.TireWheelClaim.passengerRear
})

const mapDispatchToProps = {
    createPassangerRearData: createPassangerRearData,
    createDriveFrontData: createDriveFrontData,
    createDriveRearData: createDriveRearData,
    createPassangerFrontData: createPassangerFrontData
}


export default connect(mapStateToProps, mapDispatchToProps)(TireWheelClaimDetails)