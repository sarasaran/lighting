import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Container } from 'native-base';
import { backgroundColor, loginMenuHeaderText, loginMenuCardContainer } from '../styling';
import { TitleOnlyHeader, LoginMenuCard, AfterLoginHeader3 } from '../components';


export default class LoginMenu extends Component {

    render() {
        return (
            <Container>
                <AfterLoginHeader3 title={'Lightning'} />
                <View style={{ backgroundColor: backgroundColor, flex: 1 }}>
                    <Text style={loginMenuHeaderText}>Please make a selection</Text>
                    <View style={loginMenuCardContainer}>
                        <LoginMenuCard onPress={() => this.props.navigation.navigate('AdminLogin')} text={'Continue as Admin'} />
                        <LoginMenuCard onPress={() => this.props.navigation.navigate('AdvisorLogin')} text={'Continue as Advisor'} />
                    </View>
                </View>
            </Container>
        )
    }
}