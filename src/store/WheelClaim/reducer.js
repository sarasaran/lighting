import * as types from './actionTypes'

const initialState = {
    issuetype: "Wheel",
    damagerepairable2: true,
    damagelocation2: "",
    reasonforwheelnonrepairable: "",
    wheeldiameter2: "Wheel Diameter",
    treaddepth2: "Tread Depth",
    wheelbrand: "",
    wheelmodel: "",
    damagetype2: "Type of Damage",
    wheeltype: "Wheel Type",
    damagearea2: "Damage Area",
    partnumber2: "",
    wheelfinishdescription: "",
    partscost2: "",
    laborcost2: "",
    othercost2: "",
    othercostdescription2: "",
    tax2: ""
}

const WheelClaim = (state = initialState, action) => {
    switch (action.type) {
        case types.DAMAGE_REPAIRABLE_CHANGE2:
            return { ...state, damagerepairable2: action.data }
        case types.REASON_TEXT_CHANGE2:
            return { ...state, reasonforwheelnonrepairable: action.data }
        case types.WHEEL_DIAMETER2:
            return { ...state, wheeldiameter2: action.data }
        case types.TREAD_DEPTH2:
            return { ...state, treaddepth2: action.data }
        case types.WHEEL_BRAND:
            return { ...state, wheelbrand: action.data }
        case types.WHEEL_MODEL:
            return { ...state, wheelmodel: action.data }
        case types.TYPES_OF_DAMAGE2:
            return { ...state, damagetype2: action.data }
        case types.WHEEL_TYPE:
            return { ...state, wheeltype: action.data }
        case types.DAMAGE_AREA2:
            return { ...state, damagearea2: action.data }
        case types.PART_NUMBER2:
            return { ...state, partnumber2: action.data }
        case types.WHEEL_FINISH_DESCRIPTION:
            return { ...state, wheelfinishdescription: action.data }
        case types.PART_COST2:
            return { ...state, partscost2: action.data }
        case types.LABOR_COST2:
            return { ...state, laborcost2: action.data }
        case types.OTHER_COST2:
            return { ...state, othercost2: action.data }
        case types.OTHER_COST_DESCRIPTION2:
            return { ...state, othercostdescription2: action.data }
        case types.TAX2:
            return { ...state, tax2: action.data }
        default:
            return state
    }
}

export default WheelClaim
