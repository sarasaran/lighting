import { AsyncStorage } from 'react-native';
import * as types from './actionTypes'
import axios from 'axios';
import { apiSource } from '../../constants';

const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});

export const getVINInfo = (contractid) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.get(`getcontractinfo?contractid=${contractid}`)
            .then(response => {
                let data = response.data
              
                if (data) {
                    dispatch(serviceActionSuccess(data))
                } else {
                    dispatch(serviceActionError(data))
                }
            })
            .catch(error => {
                dispatch(serviceActionError(error.response.data[0]))
            });
    }
}

const serviceActionPending = () => ({
    type: types.SERVICE_PENDING_VIN
})

const serviceActionSuccess = (data) => ({
    type: types.SERVICE_SUCCESS_VIN,
    data
})

const serviceActionError = (error) => ({
    type: types.SERVICE_ERROR_VIN,
    error
})