import * as types from './actionTypes'

const initialState = {
    isLoading: false,
    error: null,
    message: null,
    driverFront: null,
    passengerFront: null,
    driverRear: null,
    passengerRear: null,
    claimImages0: null,
    claimImages1: null,
    claimImages2: null,
    claimImages3: null
}

const claimApproval = (state = initialState, action) => {
    
   
    switch (action.type) {
        
        
        case types.SERVICE_PENDING_DAS:
            return { ...state, isLoading: true, message: null, error: null }
        case types.SERVICE_ERROR_DAS:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_SUCCESS_DAS:
            return {
                ...state, passengerRear: null, driverRear: null, passengerFront: null, driverFront: null, error: null,
                isLoading: false, claimImages0: null, claimImages1: null, claimImages2: null, claimImages3: null, message: action.data
            }
        default:
            return state
    }
}

export default claimApproval
