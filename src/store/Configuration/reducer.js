import * as types from './actionTypes'

const initialState = {
    isLoading: false,
    error: null,
    actionData: null,
    configData: null
}

const Configuration = (state = initialState, action) => {
    switch (action.type) {
        case types.SERVICE_GET_LOCAL:
            return { ...state, configData: action }
        case types.SERVICE_PENDING_CONF:
            return { ...state, isLoading: true, error: null, actionData: null }
        case types.SERVICE_SUCCESS_CONF:
            return { ...state, isLoading: false, actionData: action }
        case types.SERVICE_ERROR_CONF:
            return { ...state, isLoading: false, error: action }
        default:
            return state
    }
}

export default Configuration
