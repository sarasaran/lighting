import { AsyncStorage } from 'react-native';
import * as types from './actionTypes'
import axios from 'axios';
import { apiSource } from '../../constants';

const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});

export const register = (user) => {
    
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('register', user)
            .then(response => {
                let data = response.data
               
                if (data) {
                    if (data.success) {
                        dispatch(serviceActionSuccess(data))
                    } else {
                        dispatch(serviceActionError(data))
                    }
                }
            })
            .catch(error => {
                dispatch(serviceActionError(error.response.data[0]))
            });
    }
}

export const saveConfigurationLocal = (config) => {
    return (dispatch) => {
        
        AsyncStorage.setItem('configurationData', JSON.stringify(config))
        dispatch(serviceGetConfigData(config))
    }
}

export const getConfigData = () => {
    return (dispatch) => {
        AsyncStorage.getItem('configurationData', (error, data) => {
            if (data) {
                data = JSON.parse(data)
            }
            console.log("localStorage", data)
            dispatch(serviceGetConfigData(data))
        })
    }
}

const serviceGetConfigData = (data) => ({
    type: types.SERVICE_GET_LOCAL,
    data
})

const serviceActionPending = () => ({
    type: types.SERVICE_PENDING_CONF
})

const serviceActionSuccess = (data) => ({
    type: types.SERVICE_SUCCESS_CONF,
    data
})

const serviceActionError = (error) => ({
    type: types.SERVICE_ERROR_CONF,
    error
})