import * as types from './actionTypes'

const initialState = {
    isLoading: false,
    error: null,
    claimData: null
}

const ClaimIntent = (state = initialState, action) => {
    switch (action.type) {
        case types.SERVICE_PENDING_CLAIM:
            return { ...state, isLoading: true }
        case types.SERVICE_SUCCESS_CLAIM:
            return { ...state, isLoading: false, claimData: action }
        case types.SERVICE_ERROR_CLAIM:
            return { ...state, isLoading: false, error: action }
        default:
            return state
    }
}

export default ClaimIntent
