import * as types from './actionTypes'

const initialState = {
    isLoggedIn: false,
    isLoading: false,
    user: null,
    error: null
}

const AdminLogin = (state = initialState, action) => {
    switch (action.type) {
        case types.SERVICE_PENDING_AL:
            return { ...state, isLoading: true }
        case types.SERVICE_SUCCESS_AL:
            return { ...state, isLoading: false, isLoggedIn: true, user: action }
        case types.SERVICE_ERROR_AL:
            return { ...state, isLoading: false, error: action }
        case types.LOGOUT:
            return { ...state, isLoggedIn: false, isLoading: false, user: null, error: null }
        default:
            return state
    }
}

export default AdminLogin
