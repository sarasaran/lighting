import * as types from './actionTypes'
import axios from 'axios';
import { apiSource } from '../../constants';
import { NavigationActions } from 'react-navigation'

const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});

export const login = (user) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('authenticate', user)
            .then(response => {
                let data = response.data
                if (data) {
                    if (data.result) {
                        dispatch(serviceActionSuccess(data))
                    } else {
                        dispatch(serviceActionError(data))
                    }
                }
            })
            .catch(error => {
                dispatch(serviceActionError(error.response.error.data))
            });
    }
}

export const logout = () => {
    return (dispatch) => {
        dispatch(logoutUser())
    }
}

const loginSuccess = () => ({
    type: types.LOGIN_SUCCESS
})

const logoutUser = () => ({
    type: types.LOGOUT
})

const serviceActionPending = () => ({
    type: types.SERVICE_PENDING_AL
})

const serviceActionSuccess = (data) => ({
    type: types.SERVICE_SUCCESS_AL,
    data
})

const serviceActionError = (error) => ({
    type: types.SERVICE_ERROR_AL,
    error
})