import * as types from './actionTypes';
import axios from 'axios';
import { apiSource } from '../../constants';

const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});

export const getActive = active => {
    return (dispatch) => {
        dispatch(activeClaim(active))
    }
}

export const createDriveFrontData = (data, image) => {
    return (dispatch) => {
        dispatch(driverFront(data, image))
    }
}

export const createDriveRearData = (data, image) => {
    return (dispatch) => {
        dispatch(driverRear(data, image))
    }
}

export const createPassangerFrontData = (data, image) => {
    return (dispatch) => {
        dispatch(passangerFront(data, image))
    }
}

export const createPassangerRearData = (data, image) => {
    return (dispatch) => {
        dispatch(passangerRear(data, image))
    }
}

export const removeDriveFrontData = data => {
    return (dispatch) => {
        dispatch(driverFront(null))
    }
}

export const removeDriveRearData = data => {
    return (dispatch) => {
        dispatch(driverRear(null))
    }
}

export const removePassangerFrontData = data => {
    return (dispatch) => {
        dispatch(passangerFront(null))
    }
}

export const removePassangerRearData = data => {
    return (dispatch) => {
        dispatch(passangerRear(null))
    }
}

export const submitClaim = (body) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('createclaim', body)
            .then(response => {
                let data = response.data
               
                if (data) {
                    dispatch(claimSubmission(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

const activeClaim = (active) => ({
    type: types.GET_ACTIVE_SIDE,
    active
})

const driverFront = (data, image) => ({
    type: types.CREATE_CLAIM_DRIVER_FRONT,
    data,
    image
})

const driverRear = (data, image) => ({
    type: types.CREATE_CLAIM_DRIVER_REAR,
    data,
    image
})

const passangerFront = (data, image) => ({
    type: types.CREATE_CLAIM_PASSANGER_FRONT,
    data,
    image
})

const passangerRear = (data, image) => ({
    type: types.CREATE_CLAIM_PASSANGER_REAR,
    data,
    image
})

const claimSubmission = (data) => ({
    type: types.CREATE_NEW_CLAIM,
    data
})

const serviceActionPending = () => ({
    type: types.CREATE_CLAIM_PENDING
})

const serviceActionError = (error) => ({
    type: types.CREATE_CLAIM_ERROR,
    error
})