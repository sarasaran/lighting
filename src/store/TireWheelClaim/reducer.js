import * as types from './actionTypes'

const initialState = {
    isLoading: false,
    error: null,
    message: null,
    activeClaim: 0,
    driverFront: null,
    passengerFront: null,
    driverRear: null,
    passengerRear: null,
    claimImages0: null,
    claimImages1: null,
    claimImages2: null,
    claimImages3: null
}

const TireWheelClaim = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_ACTIVE_SIDE:
            return { ...state, activeClaim: action.active, message: null }
        case types.CREATE_CLAIM_DRIVER_FRONT:
            return { ...state, driverFront: action.data, claimImages0: action.image }
        case types.CREATE_CLAIM_DRIVER_REAR:
            return { ...state, driverRear: action.data, claimImages2: action.image }
        case types.CREATE_CLAIM_PASSANGER_FRONT:
            return { ...state, passengerFront: action.data, claimImages1: action.image }
        case types.CREATE_CLAIM_PASSANGER_REAR:
            return { ...state, passengerRear: action.data, claimImages3: action.image }
        case types.CREATE_CLAIM_PENDING:
            return { ...state, isLoading: true, message: null, error: null }
        case types.CREATE_CLAIM_ERROR:
            return { ...state, isLoading: false, error: action }
        case types.CREATE_NEW_CLAIM:
            return {
                ...state, passengerRear: null, driverRear: null, passengerFront: null, driverFront: null, error: null,
                isLoading: false, claimImages0: null, claimImages1: null, claimImages2: null, claimImages3: null, message: action.data
            }
        default:
            return state
    }
}

export default TireWheelClaim
