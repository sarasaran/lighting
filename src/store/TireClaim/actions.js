import * as types from './actionTypes';

export const onValueChange = (data, type) => {
    return (dispatch) => {
        dispatch(on_change(data, type))
    }
}

const on_change = (data, type) => ({
    type: type,
    data
})

