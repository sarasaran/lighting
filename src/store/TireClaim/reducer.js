import * as types from './actionTypes'

const initialState = {
    issuetype: "Tire",
    damagerepairable: true,
    reasonfortirenonrepairable: "",
    damagelocation: "",
    damagetype: "Type of Damage",
    damagearea: "Damage Area",
    tirewidth: "Tire Width",
    tireaspectratio: "Tire Aspect Ratio",
    wheeldiameter: "Wheel Diameter",
    speedrating: "Speed Rating",
    treaddepth: "Tread Depth",
    partnumber: "",
    tirebrand: "Tire Brand",
    tiremodel: "",
    partscost: "",
    laborcost: "",
    othercost: "",
    othercostdescription: "",
    tax: ""
}

const TireClaim = (state = initialState, action) => {
    switch (action.type) {
        case types.DAMAGE_REPAIRABLE_CHANGE:
            return { ...state, damagerepairable: action.data, }
        case types.REASON_TEXT_CHANGE:
            return { ...state, reasonfortirenonrepairable: action.data }
        case types.TYPES_OF_DAMAGE:
            return { ...state, damagetype: action.data }
        case types.DAMAGE_AREA:
            return { ...state, damagearea: action.data }
        case types.TIRE_WIDTH:
            return { ...state, tirewidth: action.data }
        case types.TIRE_ASPECT_RATIO:
            return { ...state, tireaspectratio: action.data }
        case types.WHEEL_DIAMETER:
            return { ...state, wheeldiameter: action.data }
        case types.SPEED_RATING:
            return { ...state, speedrating: action.data }
        case types.TREAD_DEPTH:
            return { ...state, treaddepth: action.data }
        case types.PART_NUMBER:
            return { ...state, partnumber: action.data }
        case types.TIRE_BRAND:
            return { ...state, tirebrand: action.data }
        case types.TIRE_MODEL:
            return { ...state, tiremodel: action.data }
        case types.PART_COST:
            return { ...state, partscost: action.data }
        case types.LABOR_COST:
            return { ...state, laborcost: action.data }
        case types.OTHER_COST:
            return { ...state, othercost: action.data }
        case types.OTHER_COST_DESCRIPTION:
            return { ...state, othercostdescription: action.data }
        case types.TAX:
            return { ...state, tax: action.data }
        default:
            return state
    }
}

export default TireClaim
