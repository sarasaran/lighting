import { combineReducers } from 'redux';
import AdminLogin from './AdminLogin';
import Configuration from './Configuration';
import Dashboard from './Dashboard';
import ClaimIntent from './ClaimIntent';
import TireWheelClaim from './TireWheelClaim';
import TireClaim from './TireClaim';
import WheelClaim from './WheelClaim';
import VINInfo from './VINInfo';
import claimApproval from './claimApproval';
export default combineReducers({
    AdminLogin, Configuration, Dashboard, ClaimIntent, TireWheelClaim, TireClaim, WheelClaim,VINInfo,claimApproval
})
