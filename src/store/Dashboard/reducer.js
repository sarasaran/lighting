import * as types from './actionTypes'

const initialState = {
    isLoading: false,
    dashData: null,
    advisors: null,
    error: null,
    message: null,
    isRemovable: null
}

const Dashboard = (state = initialState, action) => {
    switch (action.type) {
        case types.SERVICE_PENDING_DAS:
            return { ...state, isLoading: true, message: null, error: null, isRemovable: null }
        case types.SERVICE_SUCCESS_DAS:
            return { ...state, isLoading: false, dashData: action.data }
        case types.SERVICE_ERROR_DAS:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_GET_ADVISOR_SUCCESS:
            return { ...state, isLoading: false, advisors: action.data.advisors }
        case types.SERVICE_GET_ADVISOR_FAILURE:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_ADDADVISOR_SUCCESS:
            return { ...state, isLoading: false, message: action }
        case types.SERVICE_ADDADVISOR_FAILURE:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_UPDATE_ADVISOR_SUCCESS:
            return { ...state, isLoading: false, message: action }
        case types.SERVICE_UPDATE_ADVISOR_FAILURE:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_DELETE_ADVISOR_SUCCESS:
            return { ...state, isLoading: false, message: action }
        case types.SERVICE_DELETE_ADVISOR_FAILURE:
            return { ...state, isLoading: false, error: action }
        case types.SERVICE_DELETABLE_ADVISOR_SUCCESS:
            return { ...state, isLoading: false, isRemovable: action }
        default:
            return state
    }
}

export default Dashboard
