import * as types from './actionTypes'
import axios from 'axios';
import { apiSource } from '../../constants';

const api = axios.create({
    baseURL: apiSource,
    timeout: 200000,
    responseType: 'json',
});

export const getDashboardData = (user) => {
    console.log(user.dealerKey);
    console.log(user.contactid)
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.get(`getsummary?dealerkey=${user.dealerkey}&contactid=${user.contactid}`)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(serviceActionSuccess(data))
                } else {
                    dispatch(serviceActionError(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

export const getAdvisors = (dealerKey) => {
  
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.get(`getadvisors?dealerkey=${dealerKey}`)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(getAdvisorSuccess(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

export const addAdvisor = (body) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('register', body)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(advisorAddedSuccess(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

export const updateAdvisor = (body) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('updateadvisor', body)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(advisorUpdateSuccess(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

export const deleteAdvisor = (body) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.post('deleteadvisor', body)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(deleteAdvisorSuccess(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

export const isAdvisorRemovable = (body) => {
    return (dispatch) => {
        dispatch(serviceActionPending())
        api.get(`isadvisorremovable?contactid=${body}`)
            .then(response => {
                let data = response.data
                console.log("response", response)
                if (data) {
                    dispatch(isDeletableAdvisorSuccess(data))
                }
            })
            .catch(error => {
                console.log("API error dashboard", error.response)
                dispatch(serviceActionError(error.response.data))
            });
    }
}

const serviceActionPending = () => ({
    type: types.SERVICE_PENDING_DAS
})

const serviceActionSuccess = (data) => ({
    type: types.SERVICE_SUCCESS_DAS,
    data
})

const serviceActionError = (error) => ({
    type: types.SERVICE_ERROR_DAS,
    error
})

const advisorAddedSuccess = (data) => ({
    type: types.SERVICE_ADDADVISOR_SUCCESS,
    data
})

const advisorUpdateSuccess = (data) => ({
    type: types.SERVICE_UPDATE_ADVISOR_SUCCESS,
    data
})

const getAdvisorSuccess = (data) => ({
    type: types.SERVICE_GET_ADVISOR_SUCCESS,
    data
})

const deleteAdvisorSuccess = (data) => ({
    type: types.SERVICE_DELETE_ADVISOR_SUCCESS,
    data
})

const isDeletableAdvisorSuccess = (data) => ({
    type: types.SERVICE_DELETABLE_ADVISOR_SUCCESS,
    data
})
