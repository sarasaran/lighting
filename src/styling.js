import { Dimensions, Platform } from 'react-native';
import isTablet from './utility/deviceType';
import { normalize } from './utility/ResponssiveFont';

const dWidth = Dimensions.get('window').width;
const dHeight = Dimensions.get('window').height;

const fontSize = isTablet ? 20 : 18
const fieldHeight = isTablet ? 50 : 40

const backgroundColor = '#5F9AB0'
const buttonColor = '#47525E'

const container = { flex: 1, backgroundColor: '#fff' }

const burgerDiv = { width: isTablet ? 54 : 40, height: isTablet ? 44 : 32, alignItems: 'center', justifyContent: 'center', backgroundColor: '#969FAA', position: 'absolute', left: 0, top: 0, zIndex: 100 }
const burgerImage = { width: isTablet ? 32 : 25, height: isTablet ? 32 : 25 }

const loginMenuHeaderText = { color: '#fff', fontSize: isTablet ? 26 : 20, width: '100%', textAlign: 'center', marginVertical: isTablet ? 40 : 20 }
const loginMenuCardContainer = { flexDirection: 'row', width: '100%', justifyContent: 'space-around' }
const loginMenuCard = { backgroundColor: buttonColor, width: '40%', paddingVertical: 20, borderRadius: 3 }

const adminLoginContainer = { backgroundColor: backgroundColor, alignItems: 'center', flex: 1, padding: 20 }

const buttonText = { color: '#fff', fontSize: 18, fontWeight: 'bold', width: '100%', textAlign: 'center', paddingVertical: isTablet ? 15 : 10 }
const buttonDiv = { width: '50%', backgroundColor: buttonColor, justifyContent: 'center', alignItems: 'center', borderRadius: 3, marginTop: 20 }
const customInput = { fontSize: fontSize, backgroundColor: 'white', height: fieldHeight, marginTop: 10, width: '80%', paddingHorizontal: 5 }
const customInputTitle = { width: '25%', fontSize: isTablet ? 20 : 16, textAlign: 'right', marginRight: '5%', color: '#fff' }

const advisorManagerTopText = { height: isTablet ? 50 : 35, backgroundColor: 'rgb(188,188,188)', color: 'rgba(0,0,0,0.5)', fontSize: fontSize, textAlign: 'center', padding: 6 }
const advisorManagerContainer = { backgroundColor: backgroundColor, alignItems: 'center', justifyContent: 'space-between', flex: 1, paddingBottom: 20 }

const addAdvisorContainer = { backgroundColor: backgroundColor, flex: 1, padding: isTablet ? 40 : 20 }
const addAdvisorImageView = { width: isTablet ? 120 : 60, height: isTablet ? 125 : 65, marginTop: 20, backgroundColor: '#C8C8C8', padding: isTablet ? 15 : 5 }

const dashBoardCardContainer = { width: isTablet ? '65%' : '90%', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around', padding: 20 }
const dashBoardHighlightedText = { backgroundColor: '#969FAA', color: '#47525E', fontSize: isTablet ? 25 : 18, padding: 10, textAlign: 'center' }
const noclaimsDiv = { width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }
const noclaimsText = { fontSize: 18, color: '#ccc' }

export {
    container, dHeight, dWidth, fontSize,
    burgerDiv, burgerImage,
    backgroundColor,
    loginMenuCard, loginMenuHeaderText, loginMenuCardContainer,
    adminLoginContainer,
    buttonText, buttonDiv, customInput, customInputTitle,
    advisorManagerTopText, advisorManagerContainer,
    addAdvisorContainer, addAdvisorImageView,
    dashBoardCardContainer, dashBoardHighlightedText, noclaimsDiv, noclaimsText
}