import React, { Component } from 'react';
import { View, SafeAreaView, Text } from 'react-native';
import { Root } from 'native-base';
import { Provider } from 'react-redux';
import { createStackNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation';
import { configureStore } from './store'
import { container } from './styling';
import { SideMenu } from './components';

import {
    LoginMenu,
    AdminLogin,
    AdvisorLogin,
    AdvisorManager,
    AddAdvisor,
    Dashboard,
    AdvisorDashboard,
    Configuration,
    ClaimIntent,
    TireWheelClaim,
    TireWheelClaimDetails,
    VINInfo,
    scanner,
    claimApproval
} from './scenes'
import isTablet from './utility/deviceType';

const store = configureStore()

const BeforeLogin = createStackNavigator({
    LoginMenu: {
        screen: LoginMenu
    },
    AdminLogin: {
        screen: AdminLogin
    },
    Configuration: {
        screen: Configuration
    },
    AdvisorLogin: {
        screen: AdvisorLogin
    }
},
    {
        headerMode: 'none',
    });

const AfterLogin = createStackNavigator({
    Dashboard: {
        screen: Dashboard,
    },
    AdvisorDashboard: {
        screen: AdvisorDashboard
    },
    AdvisorManager: {
        screen: AdvisorManager
    },
    AddAdvisor: {
        screen: AddAdvisor
    },
    ClaimIntent: {
        screen: ClaimIntent
    },
    TireWheelClaim: {
        screen: TireWheelClaim
    },
    TireWheelClaimDetails: {
        screen: TireWheelClaimDetails
    },
    VINInfo:{
        screen:VINInfo
    },
    scanner:{
        screen:scanner
    },
    claimApproval:{
        screen:claimApproval
    }
},
    {
        headerMode: 'none',
    });

const LoggedIn = createDrawerNavigator({
    home: {
        screen: AfterLogin
    }
},
    {
        drawerWidth: isTablet ? 180 : 120,
        drawerPosition: 'left',
        contentComponent: SideMenu
    });

const Stack = createStackNavigator({
    BeforeLogin: {
        screen: BeforeLogin,
    },
    LoggedIn: {
        screen: LoggedIn
    }
},
    {
        headerMode: 'none',
    });

const Rootc = createAppContainer(Stack);


export default class App extends Component {
    
    render() {
        return (
            <Root>
                <Provider store={store}>
                    <View style={container}>
                        <Rootc />
                    </View>
                </Provider>
            </Root>
        );
    }
}