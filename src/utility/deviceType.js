import DeviceInfo from 'react-native-device-info';
import Expo from "expo";
const isTablet = true;
const device=Expo.Constants.platform;

if(device.android)
{
    
    if(device.android.userInterfaceIdiom=='tablet')
    { isTablet = true;

    }
    else
    {
        isTablet = false;
    }
}
else if(device.ios)
{
    if(device.ios.userInterfaceIdiom=='tablet')
    { isTablet = true;

    }
    else
    {
        isTablet = false;
    }
}

//Expo.Constants.isTablet;//DeviceInfo.isTablet;
 
 
export default isTablet