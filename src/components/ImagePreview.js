import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import isTablet from '../utility/deviceType';
import { fontSize } from '../styling';


const container = { flex: 1, zIndex: 101, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }
const topDiv = { backgroundColor: 'rgba(71,82,94,0.8)', flex: 1, alignItems: 'center', justifyContent: 'center' }
const imageSizeSmall = { width: '60%', height: '60%' }
const imagePickerPopup = { dWidth: '100%', backgroundColor: '#fff' }
const buttonStyle = { borderTopColor: '#7E55F3', borderTopWidth: 1, width: '50%', paddingVertical: isTablet ? 20 : 10, justifyContent: 'center', alignItems: 'center' }
const buttonTextStyle = { fontSize: fontSize, textAlign: 'center', paddingVertical: isTablet ? 5 : 2 }



export default class ImagePreview extends Component {

    render() {
        const { showImagePreview, backDrop, image, onYes, cancel } = this.props;
        const imageData = image ? { uri: `data:${image.mime};base64,${image.data}` } : null;

        if (!showImagePreview) {
            return null
        }
        return (
            <View style={container}>
                <TouchableOpacity style={topDiv} onPress={backDrop} >
                    <Image source={imageData} style={imageSizeSmall} resizeMode='contain' />
                </TouchableOpacity>
                <View style={imagePickerPopup} >
                    <View style={{ padding: isTablet ? 20 : 10 }}>
                        <Text style={buttonTextStyle}>retake this photo?</Text>
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row' }}>
                        <TouchableOpacity style={[buttonStyle, { borderRightColor: '#7E55F3', borderRightWidth: 1 }]} onPress={cancel}>
                            <Text style={buttonTextStyle}>NO</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={buttonStyle} onPress={onYes}>
                            <Text style={buttonTextStyle}>YES</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}