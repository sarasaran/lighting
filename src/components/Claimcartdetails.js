import React, { Component } from 'react';
import { TouchableOpacity, Text, View, Image } from 'react-native';
import { dWidth } from '../styling';
import isTablet from '../utility/deviceType';
import { deleteIcon } from '../assets';

const width = isTablet ? dWidth / 2 - 40 : dWidth / 2 - 20
const mainDiv = { width: width, height: width - 30, borderColor: '#8492A6', borderRadius: 0.5 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }
const verySmallFont = { color: '#8190A5', fontSize: isTablet ? 15 : 7 }

export default class Claimcartdetails extends Component {

    render() {

        const { align, title, style, data, onPress, removePress,backgroundstyle,total } = this.props;
        var pad = align == 'left' ? { paddingLeft: isTablet ? 10 : 5 } : { paddingRight: isTablet ? 10 : 5 }
        var margin = align == 'right' ? { marginLeft: isTablet ? 70 : 5 } : { marginRight: isTablet ? 70 : 5 }
        var margin2 = align == 'right' ? { marginLeft: isTablet ? 70 : 25 } : { marginRight: isTablet ? 70 : 25 }
      var background ={backgroundColor: backgroundstyle};
        var float = align == 'left' ? { right: isTablet ? 10 : 5 } : { left: isTablet ? 10 : 5 };
  
        return (
            <View style={[mainDiv, background, style]}>
                
                <Text style={[smallText, pad, { textAlign: align, }]}>{title}</Text>
                
                
                    <TouchableOpacity onPress={onPress} style={[{ flex: 1 }, margin]}>
                    <View>
                    {data.length>0 && data[0].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Tire Size'}</Text>
                            <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].tirewidth}</Text>
                        </View>:null}
                        {data.length>0 && data[0].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
                            <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].tirebrand}</Text>
                        </View>:null}
                        {data.length>0 && data[0].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
                            <View>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Part Cost'}</Text>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Labor Cost'}</Text>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Other Cost'}</Text>
                            </View>
                            <View>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].partscost}</Text>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].laborcost}</Text>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].othercost}</Text>
                            </View>
                        </View>:null}
                        {data.length>0 && data[0].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Wheel Diameter'}</Text>
                            <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].wheeldiameter}</Text>
                        </View>:null}
                        {data.length>0 && data[0].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
                            <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].wheelbrand}'/'+{data[0].wheelmodel}</Text>
                        </View>:null}
                        {data.length>0 && data[0].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
                            <View>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Part Cost'}</Text>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Labor Cost'}</Text>
                                <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Other Cost'}</Text>
                            </View>
                            <View>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].partscost}</Text>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].laborcost}</Text>
                                <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[0].othercost}</Text>
                            </View>
                        </View>:null}

{data.length==2 && data[1].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Tire Size'}</Text>
<Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].tirewidth}</Text>
</View>:null}
{data.length==2 && data[1].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
<Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].tirebrand}</Text>
</View>:null}
{data.length==2 && data[1].issuetype=='Tire'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
<View>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Part Cost'}</Text>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Labor Cost'}</Text>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Other Cost'}</Text>
</View>
<View>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].partscost}</Text>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].laborcost}</Text>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].othercost}</Text>
</View>
</View>:null}
{data.length==2 && data[1].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Wheel Diameter'}</Text>
<Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].wheeldiameter}</Text>
</View>:null}
{data.length==2 && data[1].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
<Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].wheelbrand}/+{data[1].wheelmodel}</Text>
</View>:null}
{data.length==2 && data[1].issuetype=='Wheel'?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
<Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
<View>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Part Cost'}</Text>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Labor Cost'}</Text>
    <Text style={[{ textAlign: 'left',right:25 }, verySmallFont, pad]}>{'Other Cost'}</Text>
</View>
<View>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].partscost}</Text>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].laborcost}</Text>
    <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>{data[1].othercost}</Text>
</View>
</View>:null}

                        </View>
                        {data.length>0?<View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Requested Total:'}</Text>
                            <Text style={[{ textAlign: 'right',right:40 }, verySmallFont, pad]}>${total}</Text>
                        </View>:null}
                    </TouchableOpacity>
                
            </View>
        )
    }
}