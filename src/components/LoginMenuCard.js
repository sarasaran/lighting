import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native'
import { loginMenuCard, loginMenuHeaderText } from '../styling';


export default class LoginMenuCard extends Component {

    render() {
        return (
            <TouchableOpacity style={loginMenuCard} onPress={this.props.onPress} >
                <Text style={loginMenuHeaderText}>{this.props.text}</Text>
            </TouchableOpacity>
        )
    }
}