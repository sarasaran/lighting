import React, { Component } from 'react';
import { TextInput, Text, View } from 'react-native';
import { customInputTitle, customInput } from '../styling';
import isTablet from '../utility/deviceType';


export default class CustomInputWithTitle extends Component {
    render() {

        const { title, style, value, maxLength, isSecure, placeholder, onChangeText, keyboardType } = this.props;

        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', marginBottom: isTablet ? 20 : 10 }}>
                <Text style={customInputTitle}>{title}</Text>
                <TextInput
                    maxLength={maxLength}
                    style={[customInput, { width: '70%', fontSize: 18, height: 40 }, style]}
                    value={value}
                    onChangeText={onChangeText}
                    placeholder={placeholder}
                    underlineColorAndroid={'transparent'}
                    secureTextEntry={isSecure}
                    keyboardType={keyboardType}
                />
            </View>
        )
    }
}