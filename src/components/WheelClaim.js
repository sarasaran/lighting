import React, { Component } from 'react';
import { View, Text, TextInput, Switch } from 'react-native';
import { } from 'native-base';
import { fontSize } from '../styling';
import { ButtonC, DropDown } from '../components';
import isTablet from '../utility/deviceType';
import { DamageArea, DamageTypes, WheelType, TreadDepth, WheelDaimeter } from '../utility/DropDownArrays';
import { connect } from 'react-redux';
import * as types from '../store/WheelClaim/actionTypes';

const multilineText = { minHeight: isTablet ? 100 : 60, width: '100%', borderColor: '#8492A6', borderWidth: 0.5, borderRadius: 2.5, marginBottom: isTablet ? 0 : 5, fontSize: fontSize, padding: isTablet ? 10 : 5, textAlignVertical: 'top', marginTop: 10 }
const highlightedText = { height: isTablet ? 62 : 40, fontSize: isTablet ? 26 : 18, textAlign: 'center', backgroundColor: '#535353', color: '#FFFFFF', padding: isTablet ? 17 : 6 }
const buttonTextStyle = { color: '#47525E', fontSize: fontSize, paddingHorizontal: 5, }
const textInputBorder = { borderWidth: 0.5, borderColor: '#8492A6', borderRadius: 3, marginTop: 10 }


class WheelClaimComponent extends Component {


    constructor(props) {
        super(props)
    }

    checkIf(data) {
        if (data) {
            var found = false
            data.map((v, i) => {
                if (v.issuetype == "Wheel") {
                    found = true
                }
            })
            return found
        }
        return false
    }

    render() {
        const { navigation, driverFront, passengerFront, driverRear, passengerRear, onToggleFile, enabled,
            onValueChange, onCopyDataClick
        } = this.props;

        const { damagerepairable, reasonforwheelnonrepairable, wheeldiameter, treaddepth, wheelbrand,
            wheelmodel, damagetype, wheeltype, damagearea, partnumber, wheelfinishdescription, partscost, laborcost,
            othercost, othercostdescription, tax } = this.props.claimData;

        var showCopy = false
        if (this.checkIf(driverFront) || this.checkIf(passengerFront) || this.checkIf(driverRear) || this.checkIf(passengerRear)) {
            showCopy = true
        }

        return (
            <View style={{ width: isTablet ? '50%' : '100%' }}>
                <Text style={highlightedText}>Wheel Claim</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                    <Text style={{ fontSize: fontSize, color: '#000000' }}>File Claim</Text>
                    <Switch value={enabled} trackColor="#47525E" tintColor="#dadada" onTintColor="#47525E" onValueChange={onToggleFile} />
                </View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ paddingHorizontal: 10, marginBottom: 20 }}>
                    {showCopy && <ButtonC style={{ backgroundColor: '#47525E', width: '65%', marginTop: 10 }} onPress={onCopyDataClick} title={'Copy Data From'} />}
                </View>
                <View style={{ backgroundColor: '#7E55F3', height: 1 }}></View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ flexDirection: 'row', padding: 10, paddingTop: 20 }}>
                    <Text style={{ fontSize: fontSize, color: '#47525E', marginRight: 20 }}>Damage Repairable?</Text>
                    <Switch value={damagerepairable} onValueChange={(toggle) => onValueChange(toggle, types.DAMAGE_REPAIRABLE_CHANGE)} trackColor="#47525E" tintColor="#dadada" onTintColor="#47525E" />
                </View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ padding: 10, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                    {!damagerepairable && <TextInput
                        value={reasonforwheelnonrepairable}
                        onChangeText={(text) => onValueChange(text, types.REASON_TEXT_CHANGE)}
                        multiline={true}
                        maxLength={500}
                        placeholder={'Why wheel is un-repairable?'}
                        style={multilineText}
                        underlineColorAndroid="transparent" />}
                    <DropDown selected={wheeldiameter} onValueChange={(v, i) => onValueChange(v, types.WHEEL_DIAMETER)} options={WheelDaimeter} style={{ width: '48%' }} />
                    <DropDown selected={treaddepth} onValueChange={(v, i) => onValueChange(v, types.TREAD_DEPTH)} options={TreadDepth} style={{ width: '48%' }} />
                    <TextInput value={wheelbrand} placeholder={'Wheel Brand'} onChangeText={(text) => onValueChange(text, types.WHEEL_BRAND)} style={[buttonTextStyle, textInputBorder, { width: '48%', paddingVertical: 13 }]} />
                    <TextInput value={wheelmodel} placeholder={'Wheel Model'} onChangeText={(text) => onValueChange(text, types.WHEEL_MODEL)} style={[buttonTextStyle, textInputBorder, { width: '48%', paddingVertical: 13 }]} />
                    <DropDown selected={damagetype} onValueChange={(v, i) => onValueChange(v, types.TYPES_OF_DAMAGE)} options={DamageTypes} style={{ width: '52%' }} />
                    <DropDown selected={wheeltype} onValueChange={(v, i) => onValueChange(v, types.WHEEL_TYPE)} options={WheelType} style={{ width: '45%' }} />
                    <DropDown selected={damagearea} onValueChange={(v, i) => onValueChange(v, types.DAMAGE_AREA)} options={DamageArea} style={{ width: '52%' }} />
                    <TextInput value={partnumber} placeholder={'Parts Number'} onChangeText={(text) => onValueChange(text, types.PART_NUMBER)} style={[buttonTextStyle, textInputBorder, { width: '60%', paddingVertical: 13 }]} />
                    <TextInput
                        value={wheelfinishdescription}
                        onChangeText={(text) => onValueChange(text, types.WHEEL_FINISH_DESCRIPTION)}
                        multiline={true}
                        maxLength={500}
                        placeholder={'Wheel Finish Description'}
                        style={multilineText}
                        underlineColorAndroid="transparent" />
                    <TextInput value={partscost} placeholder={'Parts Cost'} onChangeText={(text) => onValueChange(text, types.PART_COST)} style={[buttonTextStyle, textInputBorder, { width: '45%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={laborcost} placeholder={'Labor Cost'} onChangeText={(text) => onValueChange(text, types.LABOR_COST)} style={[buttonTextStyle, textInputBorder, { width: '45%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={othercost} placeholder={'Other Cost'} onChangeText={(text) => onValueChange(text, types.OTHER_COST)} style={[buttonTextStyle, textInputBorder, { width: '40%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={othercostdescription} placeholder={'Other Cost Description'} onChangeText={(text) => onValueChange(text, types.OTHER_COST_DESCRIPTION)} style={[buttonTextStyle, textInputBorder, { width: '55%', paddingVertical: 13 }]} />
                    <TextInput value={tax} placeholder={'Tax'} onChangeText={(text) => onValueChange(text, types.TAX)} style={[buttonTextStyle, textInputBorder, { width: '40%', paddingVertical: 13 }]} keyboardType="numeric" />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    activeClaim: state.TireWheelClaim.activeClaim,
    driverFront: state.TireWheelClaim.driverFront,
    passengerFront: state.TireWheelClaim.passengerFront,
    driverRear: state.TireWheelClaim.driverRear,
    passengerRear: state.TireWheelClaim.passengerRear,
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(WheelClaimComponent)