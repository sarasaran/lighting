import React, { Component } from 'react';
import { TouchableOpacity, Image, View, Platform } from 'react-native';
import { Header, Title, Icon } from 'native-base';
import isTablet from '../utility/deviceType';
import isIphoneX from '../utility/checkIfIphone';
import { logo, config } from '../assets';

const IS_IPHONE_X = isIphoneX()
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 60;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;


export default class AfterLoginHeader extends Component {
    render() {
        return (
            <View style={{ marginTop: STATUS_BAR_HEIGHT, flexDirection: 'row', backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', height: isTablet ? 100 : 60 ,marginTop:22}}>
                <View style={{ width: '20%', alignItems: 'flex-start' }}>
                    <Image source={logo} style={{ width: isTablet ? 130 : 70, height: isTablet ? 88 : 50, marginLeft: isTablet ? 25 : 15 }} />
                </View>
                <View style={{ width: '60%' }}>
                    <Title style={{ color: '#47525E', fontSize: isTablet ? 26 : 18, fontWeight: '400' }}>{this.props.title}</Title>
                </View>
                <View style={{ width: '20%', alignItems: 'flex-end' }}>
                    <TouchableOpacity onPress={this.props.onPress}>
                        <Image source={config} style={{ width: isTablet ? 34 : 25, height: isTablet ? 34 : 25, marginRight: isTablet ? 25 : 15 }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}