import React, { Component } from 'react';
import { View } from 'react-native';
import { Spinner } from 'native-base';


export default class Loader extends Component {

    render() {
        if (this.props.isLoading) {
            return (
                <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, zIndex: 100, backgroundColor: 'rgba(0,0,0,0.3)', justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner size={'large'} color={'white'} />
                </View >
            )
        } else {
            return null
        }

    }
}
