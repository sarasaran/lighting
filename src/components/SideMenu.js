import React, { Component } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image } from 'react-native';
import { Content } from 'native-base';
import isTablet from '../utility/deviceType';
import { backgroundColor } from '../styling';
import { user, home } from '../assets';
import { connect } from 'react-redux';
import { getAdvisors } from '../store/Dashboard';

class SideMenu extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const { getAdvisors, configData } = this.props;
        getAdvisors(configData.data.key)
    }

    _renderAdvisors = (data) => {
        if (data) {
            return data.map((v, i) => {
                return (
                    <TouchableOpacity key={'advisors' + i} style={{ paddingHorizontal: 10, paddingVertical: 5, backgroundColor: '#C8C8C8', width: isTablet ? 130 : 80, height: isTablet ? 130 : 80, marginBottom: 20, alignItems: 'center' }}>
                        <Image source={user} style={{ width: '100%', flex: 1, opacity: 0.35, marginBottom: 3 }} />
                        <Text style={{ height: 15 }}>{v.advisorname}</Text>
                    </TouchableOpacity>
                )
            })
        } else {
            return null
        }
    }

    render() {

        const { advisors } = this.props;
        return (
            <SafeAreaView style={[{ flex: 1 }, { backgroundColor: backgroundColor }]} forceInset={{ top: 'always', horizontal: 'never' }}>
                <Content>
                    <View style={{ flex: 1, alignItems: 'center', paddingVertical: 30 }}>
                        <TouchableOpacity style={{ padding: 10, backgroundColor: '#C8C8C8', width: isTablet ? 130 : 80, height: isTablet ? 130 : 80, marginBottom: 40, alignItems: 'center' }}>
                            <View style={{ width: '100%', height: '100%', backgroundColor: '#000', opacity: 0.35 }}>
                                <Image source={home} style={{ width: '100%', height: '100%', opacity: 0.35, tintColor: '#fff' }} />
                            </View>
                        </TouchableOpacity>
                        {this._renderAdvisors(advisors)}
                    </View>
                </Content>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = state => ({
    advisors: state.Dashboard.advisors,
    configData: state.Configuration.configData
})

const mapDispatchToProps = {
    getAdvisors: getAdvisors
}


export default connect(mapStateToProps, mapDispatchToProps)(SideMenu)