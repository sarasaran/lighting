import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import isTablet from '../utility/deviceType';
import { ClaimsStatusCard } from '../components';


export default class ClaimsRow extends Component {

    _renderClaims = (data) => {
        if (data) {
            return data.map((v, i) => {
                return <ClaimsStatusCard bgColor={v.statuscolor} status={v.status} time={v.ellapsedtime} key={'claimsCard' + i} />
            })
        } else {
            return null
        }
    }

    render() {
        return (
            <View style={{ borderColor: '#000000', borderWidth: 0.7, padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.props.onPress} style={{ width: '50%' }}>
                    <Text style={{ width: '100%', color: '#47525E', fontSize: isTablet ? 28 : 18, fontWeight: '400', marginVertical: isTablet ? 15 : 8 }}>{this.props.title}</Text>
                </TouchableOpacity>
                <View style={{ width: '50%' }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {this._renderClaims(this.props.claims)}
                    </ScrollView>
                </View>
            </View>
        )
    }
}