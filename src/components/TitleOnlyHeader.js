import React, { Component } from 'react';
import { Header, Body, Title } from 'native-base';
import isTablet from '../utility/deviceType';


export default class TitleOnlyHeader extends Component {
    render() {
        return (
            <Header style={{ backgroundColor: '#fff' }}>
                <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Title style={{ color: 'black', textAlign: 'center' }}>{this.props.title}</Title>
                </Body>
            </Header>
        )
    }
}
