import React, { Component } from 'react';
import { Text, TouchableOpacity, Image, View } from 'react-native';
import isTablet from '../utility/deviceType';
import { } from '../styling';
import { deleteIcon } from '../assets';
const cardDiv = { justifyContent: 'center', alignItems: 'center', marginRight: isTablet ? 40 : 20 }
const imageDiv = { borderRadius: 8, borderStyle: 'dotted', borderColor: '#343F4B', borderWidth: 1, padding: 5 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }

export default class CardImagedisplay extends Component {
    render() {
        const { align,onPress, style, title, image, textStyle, imageStyle,data,removePress } = this.props;
        const imageData = image ? image : null;
        var float = align == 'left' ? { right: isTablet ? 10 : 5 } : { left: isTablet ? 10 : 5 };
        return (
            <View>
<TouchableOpacity onPress={removePress} style={[{ position: 'absolute', top: isTablet ? 10 : 5, zIndex: 100 }, float]}>
            <Image source={deleteIcon} style={{ width: isTablet ? 32 : 18, height: isTablet ? 32 : 18 }} />
        </TouchableOpacity>
            <TouchableOpacity onPress={onPress} style={[cardDiv, style]}>
                
                <View style={[imageDiv, imageStyle]}>
                    <Image source={null} style={{ width: isTablet ? 100 : 75, height: isTablet ? 100 : 75 }} />
                </View>
            </TouchableOpacity>
            </View>
            
        )
    }
}

