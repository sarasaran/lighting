import React, { Component } from 'react';
import { View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import isTablet from '../utility/deviceType';
import { ClaimsStatusCard } from '../components';


const textModel = { textAlign: 'right', color: '#47525E', fontSize: isTablet ? 18 : 12, fontWeight: '400', marginRight: 10 }

export default class ClaimsRow2 extends Component {

    _renderClaims = (data) => {
        if (data) {
            return data.map((v, i) => {
                return <ClaimsStatusCard bgColor={v.statuscolor} status={v.status} time={v.ellapsedtime} key={'claimsCard' + i} />
            })
        } else {
            return null
        }
    }

    render() {
        return (
            <View style={{ borderColor: '#000000', borderWidth: 0.7, padding: 10, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.props.onPress} style={{ width: '70%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                    <View style={{ width: '50%' }}>
                        <Text style={{ color: '#47525E', fontSize: isTablet ? 28 : 18, fontWeight: '400',padding:5 }}>{this.props.title}</Text>
                        <Text style={{ color: '#47525E', fontSize: isTablet ? 18 : 12, fontWeight: '400',padding:5 }}>{this.props.productgroup}</Text>
                    </View>
                    <View style={{ width: '50%' }}>
                        <Text style={[textModel, { marginBottom: 10 }]}>{this.props.dealer}</Text>
                        <Text style={textModel}>Created: {this.props.createdDate}</Text>
                    </View>
                </TouchableOpacity>
                <View style={{ width: '30%' }}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        {this._renderClaims(this.props.claims)}
                    </ScrollView>
                </View>
            </View>
        )
    }
}