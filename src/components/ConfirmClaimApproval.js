import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { fontSize } from '../styling';
import isTablet from '../utility/deviceType';

const imagePickerPopup = { dWidth: '100%', backgroundColor: '#fff' }
const container = { flex: 1, zIndex: 101, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }
const topDiv = { backgroundColor: 'rgba(71,82,94,0.8)', flex: 1 }
const buttonStyle = { borderTopColor: '#7E55F3', borderTopWidth: 1, width: '50%', paddingVertical: isTablet ? 20 : 10, justifyContent: 'center', alignItems: 'center' }
const buttonTextStyle = { fontSize: fontSize, textAlign: 'center', paddingVertical: isTablet ? 5 : 2 }


export default class ConfirmClaimApproval extends Component {


    render() {
        const { visible, backDrop, cancel, submit } = this.props;
        if (!visible) {
            return null
        }
        return (
            <View style={container}>
                <TouchableOpacity style={topDiv} onPress={backDrop} >

                </TouchableOpacity>
                <View style={imagePickerPopup} >
                    <View style={{ padding: isTablet ? 20 : 10 }}>
                        <Text style={buttonTextStyle}>You are submitting {this.props.Docvalues} document for Claim Approval, </Text>
                        <Text style={buttonTextStyle}>Do you wish to submit for Claim Approval ?</Text>
                       
                    </View>
                    <View style={{ width: '100%', flexDirection: 'row' }}>
                        <TouchableOpacity style={[buttonStyle, { borderRightColor: '#7E55F3', borderRightWidth: 1,width:'30%' }]} onPress={cancel}>
                            <Text style={buttonTextStyle}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[buttonStyle, {width:'70%'}]} onPress={submit}>
                            <Text style={buttonTextStyle}>Submit for Claim Approval</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

