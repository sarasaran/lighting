import React, { Component } from 'react';
import { Text, Image, View, Platform, TouchableOpacity } from 'react-native';
import { Header, Title, Icon } from 'native-base';
import isTablet from '../utility/deviceType';
import isIphoneX from '../utility/checkIfIphone';
import { logo, back } from '../assets';

const IS_IPHONE_X = isIphoneX()
const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 60;
const NAV_BAR_HEIGHT = HEADER_HEIGHT - STATUS_BAR_HEIGHT;


export default class AfterLoginHeader2 extends Component {
    render() {
        return (
            <View style={{ marginTop: STATUS_BAR_HEIGHT, flexDirection: 'row', backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', height: isTablet ? 100 : 60,marginTop:22 }}>
                <View style={{ width: '30%', alignItems: 'flex-start' }}>
                    {!this.props.isBack && <Image source={logo} style={{ width: isTablet ? 130 : 70, height: isTablet ? 88 : 50, marginLeft: isTablet ? 25 : 15 }} />}
                    {this.props.isBack &&
                        <TouchableOpacity onPress={this.props.onPress} style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', width: isTablet ? 90 : 70, marginLeft: isTablet ? 20 : 10 }}>
                            <Icon name='arrow-back' style={{ color: '#8190A5', marginRight: 5, fontSize: isTablet ? 50 : 35 }} />
                            <Text style={{ color: '#8190A5', fontSize: isTablet ? 24 : 18 }}>Back</Text>
                        </TouchableOpacity>
                    }
                </View>
                <View style={{ width: '70%', alignItems: 'flex-end', paddingRight: 10 }}>
                    <Text style={{ fontSize: isTablet ? 26 : 18, color: '#47525E' }}>{this.props.name}</Text>
                    <Text style={{ fontSize: isTablet ? 18 : 12, color: '#47525E' }}>Service Advisor</Text>
                </View>
            </View>
        )
    }
}