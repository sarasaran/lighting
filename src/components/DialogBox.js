import React, { Component } from 'react';
import { Text, TouchableOpacity,View } from 'react-native';
import { buttonText, buttonDiv } from '../styling';
import { Dialog,ConfirmDialog,ProgressDialog } from 'react-native-simple-dialogs';
import { ButtonC } from '../components';
export default class DialogBox extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Dialog
                    // title="Custom Dialog"
                    animationType="fade"
                    contentStyle={
                        {
                            alignItems: "center",
                            justifyContent: "center",
                        }
                    }
                    onTouchOutside={ () => this.openDialog(false) }
                    visible={ this.state.showDialog }
                >
                    {/* <View style={{flex:1,flexDirection:'row'}}> */}
                    <Text style={ { alignItems: "center",
                            justifyContent: "center",fontWeight:"bold",fontSize:20 } }>
                        saranya
                    </Text>
                    <Text style={ { alignItems: "center",
                            justifyContent: "center",fontSize:18 } }>
                        please verify the customer identity
                    </Text>
                    <View style={{flexDirection:'row',justifyContent: 'space-between'}}>
                    <ButtonC
                        onPress={ () => this.openDialog(false) }
                       
                        title="Not Customer"
                    />
                    <ButtonC
                        onPress={ () => this.openDialog(false) }
                        style={{marginLeft:10}}
                        title="Confirm"
                    />
                    </View>
                </Dialog>
        )
    }
}