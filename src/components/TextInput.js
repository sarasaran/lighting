import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { customInput } from '../styling';


export default class CustomInput extends Component {
    render() {
        return (
            <TextInput
                style={[customInput, this.props.style]}
                value={this.props.value}
                onChangeText={this.props.onChangeText}
                placeholder={this.props.placeholder}
                underlineColorAndroid={'transparent'}
                secureTextEntry={this.props.isSecure}
            />
        )
    }
}