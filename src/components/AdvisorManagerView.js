import React, { Component } from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import { Icon, Content } from 'native-base';
import { } from '../styling';
import isTablet from '../utility/deviceType';

const box = { width: isTablet ? 100 : 90, height: 100, alignItems: 'center', backgroundColor: 'rgb(188,188,188)', padding: 10, marginHorizontal: isTablet ? 20 : 10, marginVertical: 10 }

export default class AdvisorManagerView extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {

    }

    renderLists = () => {
        const { dataSource } = this.props;
        if (dataSource) {
            return dataSource.map((v, i) => {
                return (
                    <TouchableOpacity key={'advisor_' + i} style={box} onPress={() => this.props.onPressAdvisor(v)}>
                        <Image style={{ width: '100%', height: 60, marginBottom: 5, opacity: 0.35 }} source={require('../assets/user.png')} />
                        <Text>{v.advisorname}</Text>
                    </TouchableOpacity>
                )
            })
        }
        return null
    }

    render() {
        return (
            <Content>
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', padding: 20 }}>
                    {this.renderLists()}
                    <TouchableOpacity style={[{ justifyContent: 'center' }, box]} onPress={this.props.onPressAdd}>
                        <Icon name="add-circle-outline" type="MaterialIcons" style={{ color: 'rgba(0,0,0,0.3)', marginBottom: 12 }} />
                        <Text style={{ color: 'rgba(0,0,0,0.3)', fontSize: 12 }}>Add Advisor</Text>
                    </TouchableOpacity>
                </View>
            </Content>
        )
    }
}