import React, { Component } from 'react';
import { View, Text } from 'react-native';
import isTablet from '../utility/deviceType';


export default class DashCards extends Component {

    render() {
        return (
            <View style={{ width: isTablet ? 210 : 120, backgroundColor: '#8296C9', borderColor: '#343F4B', borderWidth: 2.5, marginBottom: 20, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#FFFFFF', fontSize: isTablet ? 40 : 25, fontWeight: '400', textAlign: 'center', marginVertical: isTablet ? 15 : 8 }}>{this.props.topText}</Text>
                <Text style={{ color: '#FFFFFF', fontSize: isTablet ? 18 : 13, fontWeight: '400', textAlign: 'center', marginBottom: isTablet ? 15 : 8 }}>{this.props.subText}</Text>
            </View>
        )
    }
}