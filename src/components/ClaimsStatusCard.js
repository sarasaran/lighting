import React, { Component } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import isTablet from '../utility/deviceType';
import { bgcyan } from 'ansi-colors';
import { circle, circleTick } from '../assets';


export default class ClaimsStatusCard extends Component {

    render() {

        const { status, time, bgColor } = this.props;
        var icon = circle
        var color = '#fff'
        if (bgColor == 'red') {
            icon = circle
            color = '#fff'
        } else if (bgColor == 'white') {
            icon = circle
            color = '#000'
        } else {
            icon = circleTick
            color = '#000'
        }

        return (
            <View style={{ backgroundColor: bgColor, borderColor: '#000000', borderWidth: 0.5, width: isTablet ? 140 : 90, height: isTablet ? 80 : 60, marginRight: 15, padding: 3, justifyContent: 'space-between' }}>
                <View style={{ width: '100%', flexDirection: 'row' }}>
                    <View style={{ width: '80%' }}>
                        <Text style={{ fontSize: isTablet ? 18 : 15, color: color }}>{status}</Text>
                    </View>
                    <View style={{ width: '15%', alignItems: 'flex-end' }}>
                        <Image source={icon} style={{ width: 15, height: 15 }} />
                    </View>
                </View>
                <View style={{ width: '100%', alignItems: 'flex-end' }}>
                    <Text style={{ fontSize: isTablet ? 15 : 12 }}>{time}</Text>
                </View>
            </View>
        )
    }
}