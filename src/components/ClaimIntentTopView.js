import React, { Component } from 'react';
import { Text, View } from 'react-native';
import isTablet from '../utility/deviceType';

export default class ClaimIntentTopView extends Component {
    render() {
        const { customername, street, city, state, zipcode, phone, userInfoDiv, vehicleInfo } = this.props;
        return (
            <View style={{ backgroundColor: '#C5DDE6', paddingVertical: 20, paddingHorizontal: isTablet ? 20 : 5 }}>
                <Text style={{ fontSize: isTablet ? 18 : 13, color: '#343F4B' }}>Customer</Text>
                <View style={userInfoDiv}>
                    <Text style={{ fontSize: isTablet ? 32 : 26, color: '#343F4B' }}>{customername}</Text>
                    <Text style={{ fontSize: isTablet ? 20 : 18, color: '#343F4B' }}>{street}</Text>
                    <Text style={{ fontSize: isTablet ? 20 : 18, color: '#343F4B', marginVertical: isTablet ? 0 : 10 }}>{city}, {state} {zipcode} {phone}</Text>
                </View>
                <Text style={{ fontSize: isTablet ? 18 : 13, color: '#343F4B', marginTop: 10 }}>Vehicle</Text>
                <View style={userInfoDiv}>
                    <View>
                        <Text style={{ fontSize: isTablet ? 20 : 14, color: '#000000' }}>Make</Text>
                        <Text style={{ fontSize: isTablet ? 26 : 18, color: '#343F4B' }}>{vehicleInfo ? vehicleInfo.make : null}</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: isTablet ? 20 : 14, color: '#000000' }}>Model</Text>
                        <Text style={{ fontSize: isTablet ? 26 : 18, color: '#343F4B' }}>{vehicleInfo ? vehicleInfo.model : null}</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: isTablet ? 20 : 14, color: '#000000' }}>Year</Text>
                        <Text style={{ fontSize: isTablet ? 26 : 18, color: '#343F4B' }}>{vehicleInfo ? vehicleInfo.year : null}</Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: isTablet ? 20 : 14, color: '#000000' }}>VIN</Text>
                        <Text style={{ fontSize: isTablet ? 26 : 18, color: '#343F4B' }}>{vehicleInfo ? vehicleInfo.vin : null}</Text>
                    </View>
                </View>
            </View>
        )
    }
}
