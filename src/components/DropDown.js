import React, { Component } from 'react';
import { View } from 'react-native';
import { Picker, Icon } from 'native-base';
import { fontSize } from '../styling';


const textStyle = { color: '#47525E', fontSize: fontSize, maxWidth: '75%' }

export default class DropDown extends Component {

    _renderOptions = (data) => {
        if (data) {
            return data.map((v, i) => {
                return <Picker.Item label={v} value={v} key={"key" + i} />
            })
        } else {
            return null
        }
    }

    render() {
        const { selected, onValueChange, options, headerBackButtonText, style } = this.props;

        return (
            <View style={[{ borderWidth: 0.5, borderColor: '#8492A6', borderRadius: 3, paddingVertical: 5, marginTop: 10 }, style]}>
                <Picker
                    textStyle={textStyle}
                    itemTextStyle={textStyle}
                    style={{ width: '100%' }}
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    headerBackButtonText={headerBackButtonText ? headerBackButtonText : "close"}
                    selectedValue={selected}
                    onValueChange={onValueChange}
                >
                    {this._renderOptions(options)}
                </Picker>
            </View>
        )
    }
}