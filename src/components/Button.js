import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { buttonText, buttonDiv } from '../styling';

export default class ButtonC extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[buttonDiv, this.props.style]}z>
                <Text style={buttonText}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}