import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Header, Title, Icon } from 'native-base';


export default class BeforeLoginHeader extends Component {
    render() {
        return (
            <Header style={{ backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center',marginTop:22 }}>
                <View style={{ width: '15%' }}>
                    <TouchableOpacity style={{ marginLeft: 10 }} onPress={this.props.onPress}>
                        <Icon name='arrow-back' style={{ color: 'black' }} />
                    </TouchableOpacity>
                </View>
                <View style={{ width: '70%' }}>
                    <Title style={{ color: 'black' }}>{this.props.title}</Title>
                </View>
                <View style={{ width: '15%' }}></View>
            </Header>
        )
    }
}