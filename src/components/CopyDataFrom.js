import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, Text, TouchableOpacity, Image, Modal } from 'react-native';
import isTablet from '../utility/deviceType';
import { fontSize } from '../styling';
import { pointer, rightArrow } from '../assets';
import { connect } from 'react-redux';

const topDiv = { backgroundColor: 'rgba(71,82,94,0.8)', flex: 1 }
const center = { justifyContent: 'center', alignItems: 'center' }
const row = { color: '#47525E', paddingHorizontal: 20, height: 46, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }
const arrowImage = { width: 10, height: 18 }
const buttonTextStyle = { fontSize: fontSize, textAlign: 'left', paddingVertical: isTablet ? 5 : 2 }
const mainDiv = isTablet ? { width: '40%', marginTop: 200, marginLeft: 10, backgroundColor: '#fff' } : { backgroundColor: '#fff', width: '80%' }

class CopyDataFrom extends Component {

    componentDidMount() {
        const { driverFront, driverRear, passengerFront, passengerRear, type } = this.props;
        
    }

    checkIf(data, type) {
        if (data) {
            var found = false
            data.map((v, i) => {
                if (v.issuetype == (type == 1 ? "Tire" : "Wheel")) {
                    found = true
                }
            })
            return found
        }
        return false
    }

    render() {
        const { showCopyDataPopup, backDrop, type, driverFront, driverRear, passengerFront, passengerRear } = this.props;
        var f1, f2, r1, r2;
        f1 = this.checkIf(driverFront, type)
        f2 = this.checkIf(passengerFront, type)
        r1 = this.checkIf(driverRear, type)
        r2 = this.checkIf(passengerRear, type)

        return (
            <Modal
                animationType={'fade'}
                transparent={true}
                visible={showCopyDataPopup}
                presentationStyle={'overFullScreen'}
                onRequestClose={backDrop}
            >
                <TouchableWithoutFeedback onPress={backDrop} style={{ flex: 1 }}>
                    <View style={[topDiv, isTablet ? { alignItems: type == 1 ? 'flex-start' : 'flex-end' } : center]}>
                        <TouchableWithoutFeedback>
                            <View style={[mainDiv, { marginRight: type == 2 ? 10 : 0 }]}>
                                <Image source={pointer} style={{ width: 60, height: 15, tintColor: '#EFF2F7' }} />
                                <View style={[{ backgroundColor: '#EFF2F7', height: 44 }, center]}>
                                    <Text style={{ color: '#47525E', fontSize: fontSize }}>Copy Data From</Text>
                                </View>
                                <View style={{ borderColor: '#D2DAE6', borderWidth: 1 }}>
                                    {f1 &&
                                        <View>
                                            <TouchableOpacity style={row}>
                                                <Text style={buttonTextStyle}>Front Driver</Text>
                                                <Image source={rightArrow} style={arrowImage}></Image>
                                            </TouchableOpacity>
                                            <View style={{ backgroundColor: '#D2DAE6', height: 1, width: '95%', marginLeft: '5%' }}></View>
                                        </View>
                                    }
                                    {f2 &&
                                        <View>
                                            <TouchableOpacity style={row}>
                                                <Text style={buttonTextStyle}>Front Passenger</Text>
                                                <Image source={rightArrow} style={arrowImage}></Image>
                                            </TouchableOpacity>
                                            <View style={{ backgroundColor: '#D2DAE6', height: 1, width: '95%', marginLeft: '5%' }}></View>
                                        </View>
                                    }
                                    {r1 &&
                                        <View>
                                            <TouchableOpacity style={row}>
                                                <Text style={buttonTextStyle}>Rear Driver</Text>
                                                <Image source={rightArrow} style={arrowImage}></Image>
                                            </TouchableOpacity>
                                            <View style={{ backgroundColor: '#D2DAE6', height: 1, width: '95%', marginLeft: '5%' }}></View>
                                        </View>
                                    }
                                    {r2 &&
                                        <View>
                                            <TouchableOpacity style={row}>
                                                <Text style={buttonTextStyle}>Rear Passenger</Text>
                                                <Image source={rightArrow} style={arrowImage}></Image>
                                            </TouchableOpacity>
                                        </View>
                                    }
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}


const mapStateToProps = state => ({
    activeClaim: state.TireWheelClaim.activeClaim,
    driverFront: state.TireWheelClaim.driverFront,
    passengerFront: state.TireWheelClaim.passengerFront,
    driverRear: state.TireWheelClaim.driverRear,
    passengerRear: state.TireWheelClaim.passengerRear,
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(CopyDataFrom)