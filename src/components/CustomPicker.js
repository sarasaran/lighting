
import React, { Component } from 'react';
import { Picker, Text, View, Platform } from 'react-native';
import { customInputTitle, customInput } from '../styling';
import isTablet from '../utility/deviceType';


export default class CustomPicker extends Component {

    render() {
        const { title, onValueChange, value } = this.props;
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center', marginBottom: isTablet ? 20 : 10 }}>
                <Text style={customInputTitle}>{title}</Text>
                <Picker
                    itemStyle={{ height: Platform.OS == 'ios' ? 120 : 50 }}
                    selectedValue={value}
                    style={{ height: Platform.OS == 'ios' ? 120 : 50, width: '70%', backgroundColor: '#fff' }}
                    onValueChange={onValueChange}>
                    <Picker.Item label="Service Director" value="Service Director" />
                    <Picker.Item label="Service Manager" value="Service Manager" />
                    <Picker.Item label="Service Advisor" value="Service Advisor" />
                    <Picker.Item label="Warranty Administrator" value="Warranty Administrator" />
                </Picker>
            </View>
        )
    }
}