import React, { Component } from 'react';
import { Text, TouchableOpacity, Image, View } from 'react-native';
import isTablet from '../utility/deviceType';
import { } from '../styling';

const cardDiv = { justifyContent: 'center', alignItems: 'center', marginRight: isTablet ? 40 : 20 }
const imageDiv = { borderRadius: 8, borderStyle: 'dotted', borderColor: '#343F4B', borderWidth: 1, padding: 5 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }

export default class CardImage extends Component {
    render() {
        const { onPress, style, title, image, textStyle, imageStyle } = this.props;
        const imageData = image ? image : null;

        return (
            <TouchableOpacity onPress={onPress} style={[cardDiv, style]}>
                <Text style={[smallText, textStyle]}>{title}</Text>
                <View style={[imageDiv, imageStyle]}>
                    <Image source={imageData} style={{ width: isTablet ? 100 : 75, height: isTablet ? 100 : 75 }} />
                </View>
            </TouchableOpacity>
        )
    }
}