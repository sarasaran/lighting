import React, { Component } from 'react';
import { TouchableOpacity, Text, View, Image } from 'react-native';
import { dWidth } from '../styling';
import isTablet from '../utility/deviceType';
import { deleteIcon } from '../assets';

const width = isTablet ? dWidth / 2 - 40 : dWidth / 2 - 20
const mainDiv = { width: width, height: width - 40, borderColor: '#8492A6', borderRadius: 0.5 }
const smallText = { color: '#47525E', fontSize: isTablet ? 20 : 14, paddingVertical: isTablet ? 6 : 3 }
const verySmallFont = { color: '#8190A5', fontSize: isTablet ? 15 : 7 }

export default class AddDetailsCard extends Component {

    render() {

        const { align, title, style, data, onPress, removePress } = this.props;
        var pad = align == 'left' ? { paddingLeft: isTablet ? 10 : 5 } : { paddingRight: isTablet ? 10 : 5 }
        var margin = align == 'right' ? { marginLeft: isTablet ? 70 : 5 } : { marginRight: isTablet ? 70 : 5 }
        var margin2 = align == 'right' ? { marginLeft: isTablet ? 70 : 25 } : { marginRight: isTablet ? 70 : 25 }
        var background = data ? { backgroundColor: 'rgba(31,200,83,0.27)' } : { backgroundColor: 'rgba(162,162,162,0.27)' }
        var float = align == 'left' ? { right: isTablet ? 10 : 5 } : { left: isTablet ? 10 : 5 };
        return (
            <View style={[mainDiv, background, style]}>
                {data &&
                    <TouchableOpacity onPress={removePress} style={[{ position: 'absolute', top: isTablet ? 10 : 5, zIndex: 100 }, float]}>
                        <Image source={deleteIcon} style={{ width: isTablet ? 32 : 18, height: isTablet ? 32 : 18 }} />
                    </TouchableOpacity>
                }
                <Text style={[smallText, pad, { textAlign: align, }]}>{title}</Text>
                {!data &&
                    <TouchableOpacity onPress={onPress} style={[{ flexDirection: 'row', justifyContent: 'space-around', flex: 1, alignItems: 'center' }, margin2]}>
                        <Text style={{ fontSize: isTablet ? 120 : 80, color: '#47525E' }}>[</Text>
                        <Text style={{ fontSize: isTablet ? 40 : 25, textAlign: 'center', color: '#47525E' }}>{'Add \n Details'}</Text>
                        <Text style={{ fontSize: isTablet ? 120 : 80, color: '#47525E' }}>]</Text>
                    </TouchableOpacity>
                }
                {data &&
                    <TouchableOpacity onPress={onPress} style={[{ flex: 1 }, margin]}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Tire Size'}</Text>
                            <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'275/55-20'}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
                            <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'Goodyear/Assurance'}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
                            <View>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Part Cost'}</Text>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Labor Cost'}</Text>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Other Cost'}</Text>
                            </View>
                            <View>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$125.25'}</Text>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$25.00'}</Text>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$15.00'}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Wheel Diameter'}</Text>
                            <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'19'}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Brand/Model'}</Text>
                            <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'OEM/OEM'}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Part Number'}</Text>
                            <View>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Part Cost'}</Text>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Labor Cost'}</Text>
                                <Text style={[{ textAlign: 'left' }, verySmallFont, pad]}>{'Other Cost'}</Text>
                            </View>
                            <View>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$125.25'}</Text>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$25.00'}</Text>
                                <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$15.00'}</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                            <Text style={[{ textAlign: 'left', }, verySmallFont, pad]}>{'Requested Total:'}</Text>
                            <Text style={[{ textAlign: 'right' }, verySmallFont, pad]}>{'$330.50'}</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        )
    }
}