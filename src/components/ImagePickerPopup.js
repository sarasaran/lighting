import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { dWidth, dHeight } from '../styling';
import isTablet from '../utility/deviceType';
import { cameraCircle, cross, imageIcon } from '../assets';

const bottomDivHeight = isTablet ? 80 : 60
const imagePickerPopup = { dWidth: dWidth, backgroundColor: '#C7CDCA', height: bottomDivHeight, flexWrap: 'wrap', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: isTablet ? 40 : 20 }
const container = { flex: 1, zIndex: 101, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }
const topDiv = { backgroundColor: 'rgba(71,82,94,0.8)', flex: 1 }
const imageSizeSmall = { width: isTablet ? 36 : 24, height: isTablet ? 36 : 24 }
const imageSizeLarge = { width: isTablet ? 70 : 50, height: isTablet ? 70 : 50 }

export default class ImagePickerPopup extends Component {

    render() {
        const { showImagePicker, backDrop, close, cameraClick, galleryClick } = this.props;
        if (!showImagePicker) {
            return null
        }
        return (
            <View style={container}>
                <TouchableOpacity style={topDiv} onPress={backDrop} >

                </TouchableOpacity>
                <View style={imagePickerPopup} >
                    <TouchableOpacity style={imageSizeSmall} onPress={close}>
                        <Image source={cross} style={imageSizeSmall} />
                    </TouchableOpacity>
                    <TouchableOpacity style={imageSizeLarge} onPress={cameraClick}>
                        <Image source={cameraCircle} style={imageSizeLarge} />
                    </TouchableOpacity>
                    <TouchableOpacity style={imageSizeSmall} onPress={galleryClick}>
                        <Image source={imageIcon} style={imageSizeSmall} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}