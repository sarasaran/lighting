import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, Switch } from 'react-native';
import { } from 'native-base';
import { fontSize } from '../styling';
import { ButtonC, DropDown } from '../components';
import isTablet from '../utility/deviceType';
import { DamageArea, DamageTypes, TireWidth, TireBrand, TreadDepth, SpeedRating, WheelDaimeter, TireAspectRatio } from '../utility/DropDownArrays';
import { connect } from 'react-redux';
import * as types from '../store/TireClaim/actionTypes';


const multilineText = { minHeight: isTablet ? 100 : 60, width: '100%', borderColor: '#8492A6', borderWidth: 0.5, borderRadius: 2.5, marginBottom: isTablet ? 0 : 5, fontSize: fontSize, padding: isTablet ? 10 : 5, textAlignVertical: 'top', marginTop: 10 }
const highlightedText = { height: isTablet ? 62 : 40, fontSize: isTablet ? 26 : 18, textAlign: 'center', backgroundColor: '#535353', color: '#FFFFFF', padding: isTablet ? 17 : 6 }
const border = isTablet ? { borderRightColor: '#000000', borderRightWidth: 1.5 } : {}
const buttonTextStyle = { color: '#47525E', fontSize: fontSize, paddingHorizontal: 5, }
const textInputBorder = { borderWidth: 0.5, borderColor: '#8492A6', borderRadius: 3, marginTop: 10 }
const buttonTransparent = { borderWidth: 0.5, borderColor: '#8492A6', backgroundColor: '#EBEBEB', borderRadius: 3, marginTop: 10, justifyContent: 'center', alignItems: 'flex-start' }


class TireClaim extends Component {

    constructor(props) {
        super(props)
    }

    checkIf(data, type) {
        if (data) {
            var found = false
            data.map((v, i) => {
                if (v.issuetype == "Tire") {
                    found = true
                }
            })
            return found
        }
        return false
    }

    render() {
        const { navigation, onToggleFile, enabled, driverFront, driverRear, passengerFront, passengerRear, onValueChange,
            onCopyDataClick
        } = this.props;

        const { damagerepairable, reasonfortirenonrepairable, damagetype, damagearea, tirewidth, tireaspectratio, wheeldiameter, speedrating,
            treaddepth, partnumber, tirebrand, tiremodel, partscost, laborcost, othercost, othercostdescription, tax, } = this.props.claimData;

        var showCopy = false
        if (this.checkIf(driverFront) || this.checkIf(passengerFront) || this.checkIf(driverRear) || this.checkIf(passengerRear)) {
            showCopy = true
        }

        return (
            <View style={[{ width: isTablet ? '50%' : '100%' }, border]}>
                <Text style={highlightedText}>Tire Claim</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10 }}>
                    <Text style={{ fontSize: fontSize, color: '#000000' }}>File Claim</Text>
                    <Switch value={enabled} trackColor="#47525E" tintColor="#dadada" onTintColor="#47525E" onValueChange={onToggleFile} />
                </View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ paddingHorizontal: 10, marginBottom: 20 }}>
                    {showCopy && <ButtonC style={{ backgroundColor: '#47525E', width: '65%', marginTop: 10 }} onPress={onCopyDataClick} title={'Copy Data From'} />}
                </View>
                <View style={{ backgroundColor: '#7E55F3', height: 1 }}></View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ flexDirection: 'row', padding: 10, paddingTop: 20 }}>
                    <Text style={{ fontSize: fontSize, color: '#47525E', marginRight: 20 }}>Damage Repairable?</Text>
                    <Switch value={damagerepairable} onValueChange={(toggle) => onValueChange(toggle, types.DAMAGE_REPAIRABLE_CHANGE)} trackColor="#47525E" tintColor="#dadada" onTintColor="#47525E" />
                </View>
                <View pointerEvents={enabled ? "auto" : "none"} style={{ padding: 10, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                    {!damagerepairable && <TextInput
                        value={reasonfortirenonrepairable}
                        onChangeText={(text) => onValueChange(text, types.REASON_TEXT_CHANGE)}
                        multiline={true}
                        maxLength={500}
                        placeholder={'Why tire is un-repairable?'}
                        style={multilineText}
                        underlineColorAndroid="transparent" />}
                    <DropDown selected={damagetype} onValueChange={(v, i) => onValueChange(v, types.TYPES_OF_DAMAGE)} options={DamageTypes} style={{ width: '70%' }} />
                    <DropDown selected={damagearea} onValueChange={(v, i) => onValueChange(v, types.DAMAGE_AREA)} options={DamageArea} style={{ width: '52%' }} />
                    <DropDown selected={tirewidth} onValueChange={(v, i) => onValueChange(v, types.TIRE_WIDTH)} options={TireWidth} style={{ width: '45%' }} />
                    <DropDown selected={tireaspectratio} onValueChange={(v, i) => onValueChange(v, types.TIRE_ASPECT_RATIO)} options={TireAspectRatio} style={{ width: '52%' }} />
                    <DropDown selected={wheeldiameter} onValueChange={(v, i) => onValueChange(v, types.WHEEL_DIAMETER)} options={WheelDaimeter} style={{ width: '45%' }} />
                    <DropDown selected={speedrating} onValueChange={(v, i) => onValueChange(v, types.SPEED_RATING)} options={SpeedRating} style={{ width: '52%' }} />
                    {transparentButton('Tire Size', { width: '45%' }, () => alert('clicked'))}
                    <DropDown selected={treaddepth} onValueChange={(v, i) => onValueChange(v, types.TREAD_DEPTH)} options={TreadDepth} style={{ width: '70%' }} />
                    <TextInput value={partnumber} placeholder={'Part Number'} onChangeText={(text) => onValueChange(text, types.PART_NUMBER)} style={[buttonTextStyle, textInputBorder, { width: '70%', paddingVertical: 13 }]} />
                    <DropDown selected={tirebrand} onValueChange={(v, i) => onValueChange(v, types.TIRE_BRAND)} options={TireBrand} style={{ width: '52%' }} />
                    <TextInput value={tiremodel} placeholder={'Tire Model'} onChangeText={(text) => onValueChange(text, types.TIRE_MODEL)} style={[buttonTextStyle, textInputBorder, { width: '45%', paddingVertical: 13 }]} />
                    <TextInput value={partscost} placeholder={'Parts Cost'} onChangeText={(text) => onValueChange(text, types.PART_COST)} style={[buttonTextStyle, textInputBorder, { width: '45%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={laborcost} placeholder={'Labor Cost'} onChangeText={(text) => onValueChange(text, types.LABOR_COST)} style={[buttonTextStyle, textInputBorder, { width: '45%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={othercost} placeholder={'Other Cost'} onChangeText={(text) => onValueChange(text, types.OTHER_COST)} style={[buttonTextStyle, textInputBorder, { width: '40%', paddingVertical: 13 }]} keyboardType="numeric" />
                    <TextInput value={othercostdescription} placeholder={'Other Cost Description'} onChangeText={(text) => onValueChange(text, types.OTHER_COST_DESCRIPTION)} style={[buttonTextStyle, textInputBorder, { width: '55%', paddingVertical: 13 }]} />
                    <TextInput value={tax} placeholder={'Tax'} onChangeText={(text) => onValueChange(text, types.TAX)} style={[buttonTextStyle, textInputBorder, { width: '40%', paddingVertical: 13 }]} keyboardType="numeric" />
                </View>
            </View>
        )
    }
}

const transparentButton = (text, style, onPress) => {
    return (
        <TouchableOpacity style={[buttonTransparent, style]} onPress={onPress}>
            <Text style={buttonTextStyle}>{text}</Text>
        </TouchableOpacity>
    )
}

const mapStateToProps = state => ({
    driverFront: state.TireWheelClaim.driverFront,
    passengerFront: state.TireWheelClaim.passengerFront,
    driverRear: state.TireWheelClaim.driverRear,
    passengerRear: state.TireWheelClaim.passengerRear,
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(TireClaim)