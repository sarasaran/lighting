//Import library to help create component
import React from 'react';
import { AppRegistry } from 'react-native';
import {AppLoading} from 'expo'
import Header from './src/routes';
import { getClaim } from './src/store/claimApproval';

class App extends React.Component{
  state={
    isLoading:true
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ isLoading: false });
    var body = {claimid: 'a0A0v0000015zTA' }
    getClaim(body);
  }
  render(){
    const {isLoading} = this.state;
    if(isLoading){
      return <AppLoading />
    }
    return(<Header />)
  }
}


export default App;

//Render it to the device
AppRegistry.registerComponent('lightning', () => App);
